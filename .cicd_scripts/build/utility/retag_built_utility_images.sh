#!/usr/bin/env bash
set -eu -o pipefail -E

DIR="${0%/*}"

# shellcheck source=../../other/utils.sh
source "${DIR}/../../other/utils.sh"

# shellcheck source=../common/common.sh
source "${DIR}/../common/common.sh"

# shellcheck source=./lib.sh
source "${DIR}/lib.sh"

# Registry address for Docker Hub. Non-configurable from CICD.
# Selected experimentally (index.docker.io failed on Gitlab Runners).
# shellcheck disable=SC2034
DOCKERHUB_REGISTRY="docker.io"

function check_env() {
    log_info_variable RELEASE_TAG                        "${RELEASE_TAG}"
}

check_env

# The .images/ is where the docker_data_utility.json files are stored. See build/service
retag_built_utility_images .images/ "${RELEASE_TAG}"

set +eu +o pipefail +E