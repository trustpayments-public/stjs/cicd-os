REPORT_FILE_NAME="docker_data_utility.json"

function _get_docker_registry() {
  # shellcheck disable=SC2153
  case "${DOCKER_REGISTRY}" in
    gitlab)
      echo "${CI_REGISTRY_IMAGE}"
      ;;
    dockerhub)
      echo "${DOCKERHUB_REGISTRY}"
      ;;
    *)
      log_this ERROR "Unexpected DOCKER_REGISTRY value: ${DOCKER_REGISTRY}."
      exit 1
  esac
}

function _get_docker_repository_name() {
  # Returns Docker Repository name depending on the DOCKER_REPOSITORY_NAME override.
  # Note that - unlike services - there are no snapshot/release distinction, everything lands in single repository
  # whose name is derived from UTILITY_NAME, by convention prepended with `securetrading1/` as that's what we have
  # set up on DockeHub. The prefix is also added when you specify an override. We do not allow writing to
  # DockerHub Repositories other than ours.

  # shellcheck disable=SC2153
  case "${DOCKER_REGISTRY}" in

    gitlab)
      if [[ "${DOCKER_REPOSITORY_NAME}" == "" ]]; then
        name="${UTILITY_NAME//_/-}"
      else
        name="${DOCKER_REPOSITORY_NAME}"
      fi
      ;;

    dockerhub)
      # shellcheck disable=SC2153
      if [[ "${DOCKER_REPOSITORY_NAME}" == "" ]]; then
        local clean_utility_name="${UTILITY_NAME//_/-}"
        name="securetrading1/${clean_utility_name}"
      else
        name="securetrading1/${DOCKER_REPOSITORY_NAME}"
      fi
      ;;

    *)
      log_this ERROR "Unexpected DOCKER_REGISTRY value: ${DOCKER_REGISTRY}."
      exit 1

  esac

  echo "${name}"
}

function _get_push_tag() {

  # Returns a full docker tag including repository name, image name and its tag.
  # To be used with `docker push` of to identify potentially existing/cached images for pull.

  local version_flavour="$1"

  local docker_registry
  local docker_repository_name
  local version
  local push_tag

  docker_registry="$(_get_docker_registry)"
  docker_repository_name="$(_get_docker_repository_name)"
  version="$(get_project_version "${version_flavour}")"

  push_tag="${docker_registry}/${docker_repository_name}:${version}"
  echo "${push_tag}"
}

function _get_report_file_path() {
  local report_file_location="$1"
  local repository_name
  local report_file_path

  repository_name="$(_get_docker_repository_name)"
  report_file_path="${report_file_location}/${repository_name}/${REPORT_FILE_NAME}"
  echo "${report_file_path}"
}

function build_utility_image() {

  local docker_target
  local dockerfile_path
  local build_tag

  docker_target="$(get_docker_target "${UTILITY_NAME}")"
  dockerfile_path="$(get_dockerfile_path "${UTILITY_NAME}")"
  build_tag="$(get_build_tag "${UTILITY_NAME}")"

  docker_login "${DOCKER_REGISTRY}"

  log_this INFO "Building image: ${build_tag} using: ${dockerfile_path}"

  BUILD_ARGS_EXTRA="${BUILD_ARGS_EXTRA} --no-cache"

  log_info_variable DOCKER_BUILD_ARGS_PROXY                   "${DOCKER_BUILD_ARGS_PROXY}"
  log_info_variable BUILD_ARGS_EXTRA                          "${BUILD_ARGS_EXTRA}"

  # shellcheck disable=SC2086
  docker build -t "${build_tag}"                   \
    ${DOCKER_BUILD_ARGS_PROXY} ${BUILD_ARGS_EXTRA} \
    --target "${docker_target}"                    \
    --file "${dockerfile_path}" .
}

function _generate_report() {

  local report_file_path="$1"

  local registry_id
  local repository_name
  local image_tag
  local push_tag

  registry_id="$(_get_docker_registry)"
  push_tag="$(_get_push_tag "incremental")"

  repository_name="$(_get_docker_repository_name)"
  image_tag="$(get_project_version "incremental")"

  mkdir -p "$(dirname "${report_file_path}")"

  # shellcheck disable=SC2016
  local report_template='{
        "registry":        $registry,
        "registry_id":     $registry_id,
        "repository_name": $repository_name,
        "image_tag":       $image_tag,
    }'

  jq -n "${report_template}"                     \
    --arg "registry" "${DOCKER_REGISTRY}"        \
    --arg "registry_id" "${registry_id}"         \
    --arg "repository_name" "${repository_name}" \
    --arg "image_tag" "${image_tag}"             \
    >"${report_file_path}"

}

function publish_utility_image_version_tag() {

  # See: common/common.sh :: get_project_version()
  local version_flavour="$1"

  local build_tag
  local push_tag

  build_tag="$(get_build_tag "${UTILITY_NAME}")"
  push_tag="$(_get_push_tag "${version_flavour}")"

  docker_login "${DOCKER_REGISTRY}"

  log_this INFO "Tagging build-image as: ${push_tag}"
  docker tag "${build_tag}" "${push_tag}"

  log_this INFO "Pushing image ${push_tag} to remote repository"
  docker push "${push_tag}"
}

function publish_utility_image() {

  local report_files_dir="$1"

  publish_utility_image_version_tag "incremental"

  # Generate reports and run scans for "incremental" (i.e. official) versions only.
  report_file_path="$(_get_report_file_path "${report_files_dir}")"
  _generate_report "${report_file_path}"

}

function retag_built_utility_images() {

  local scan_path="$1"
  local new_image_tag="$2"

  while read -r docker_data_file; do

    log_this INFO "Processing ${docker_data_file}"

    local registry
    local registry_id
    local repository_name
    local image_tag

    # Note: DOCKER_REGISTRY is not available here, because this script is ran from separate job,
    # and once for all built images. Thus, we need to read the docker registry type from the file, not from Env.
    registry="$(jq -r '.registry' "${docker_data_file}")"
    registry_id="$(jq -r '.registry_id' "${docker_data_file}")"
    repository_name="$(jq -r '.repository_name' "${docker_data_file}")"
    image_tag="$(jq -r '.image_tag' "${docker_data_file}")"

    echo "docker_login '${registry}'"
    docker_login "${registry}"

    docker rmi "${registry_id}/${repository_name}:${image_tag}" 2>/dev/null || true
    docker pull "${registry_id}/${repository_name}:${image_tag}"

    docker tag  "${registry_id}/${repository_name}:${image_tag}" "${registry_id}/${repository_name}:${new_image_tag}"
    docker push "${registry_id}/${repository_name}:${new_image_tag}"
    log_this INFO "Re-tagged image ${repository_name}:${image_tag} as '${new_image_tag}' in ${registry_id}."

    docker tag  "${registry_id}/${repository_name}:${image_tag}" "${registry_id}/${repository_name}:stable"
    docker push "${registry_id}/${repository_name}:stable"
    log_this INFO "Re-tagged image ${repository_name}:${image_tag} as 'stable' in ${registry_id}."

  done < <(find "${scan_path}" -type f -name "${REPORT_FILE_NAME}")

}

function _get_execution_path() {
  local original_execution_path=${1}
  local execution_path="${original_execution_path}"

  if [[ "${BUILD_EXECUTION_PATH}" != "" ]]; then
    execution_path="${BUILD_EXECUTION_PATH}"
  fi

  echo "${execution_path}"
}

function update_execution_path() {
  local original_execution_path=$(pwd)
  local execution_path=$(_get_execution_path "${original_execution_path}")
  cd "${execution_path}"
}