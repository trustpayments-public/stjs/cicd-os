#!/usr/bin/env bash
set -eu -o pipefail -E

DIR="${0%/*}"

# shellcheck source=../../other/utils.sh
source "${DIR}/../../other/utils.sh"

# shellcheck source=../common/common.sh
source "${DIR}/../common/common.sh"

# shellcheck source=./lib.sh
source "${DIR}/lib.sh"

# Registry address for Docker Hub. Non-configurable from CICD.
# Selected experimentally (index.docker.io failed on Gitlab Runners).
DOCKERHUB_REGISTRY="docker.io"

function init_defaults() {
  : "${LOCAL_BUILD:="false"}"
  : "${DOCKER_BUILD_ARGS_PROXY:=""}"
  : "${BUILD_ARGS_EXTRA:=""}"
  : "${BUILD_EXECUTION_PATH:=""}"
  : "${DOCKERFILE_PATH:=""}"
  : "${DOCKER_TARGET:=""}"
  : "${DOCKER_REGISTRY:="gitlab"}"
  : "${DOCKER_REPOSITORY_NAME:=""}"
  : "${USE_LOG_LEVEL_ARG:=false}"
  : "${INJECT_TO_PROJECT_IMAGES:=true}"
}

function check_env() {
  log_info_variable LOCAL_BUILD                               "${LOCAL_BUILD}"
  log_info_variable UTILITY_NAME                              "${UTILITY_NAME}"
  log_info_variable CI_PROJECT_NAME                           "${CI_PROJECT_NAME}"
  log_info_variable CI_PIPELINE_ID                            "${CI_PIPELINE_ID}"
  log_info_variable CI_COMMIT_REF_SLUG                        "${CI_COMMIT_REF_SLUG}"

  log_info_variable DOCKER_BUILD_ARGS_PROXY                   "${DOCKER_BUILD_ARGS_PROXY}"
  log_info_variable BUILD_ARGS_EXTRA                          "${BUILD_ARGS_EXTRA}"
  log_info_variable BUILD_EXECUTION_PATH                      "${BUILD_EXECUTION_PATH}"
  log_info_variable DOCKERFILE_PATH                           "${DOCKERFILE_PATH}"
  log_info_variable DOCKER_TARGET                             "${DOCKER_TARGET}"
  log_info_variable DOCKER_REGISTRY                           "${DOCKER_REGISTRY}"
  log_info_variable DOCKER_REPOSITORY_NAME                    "${DOCKER_REPOSITORY_NAME}"
  log_info_variable USE_LOG_LEVEL_ARG                         "${USE_LOG_LEVEL_ARG}"
  log_info_variable INJECT_TO_PROJECT_IMAGES                  "${INJECT_TO_PROJECT_IMAGES}"

  case "${DOCKER_REGISTRY}" in
    dockerhub)
      log_info_variable DOCKERHUB_USERNAME                        "${DOCKERHUB_USERNAME}"
      log_info_variable DOCKERHUB_PASSWORD                        "${DOCKERHUB_PASSWORD}"
      log_info_variable DOCKERHUB_REGISTRY                        "${DOCKERHUB_REGISTRY}"
      ;;
    gitlab)
      log_info_variable CI_REGISTRY_USER                          "${CI_REGISTRY_USER}"
      log_info_variable CI_REGISTRY_PASSWORD                      "${CI_REGISTRY_PASSWORD}"
      log_info_variable CI_REGISTRY_IMAGE                         "${CI_REGISTRY_IMAGE}"
      log_info_variable CI_REGISTRY                               "${CI_REGISTRY}"
      ;;
    *)
      log_this ERROR "Unexpected value of DOCKER_REGISTRY environment variable: ${DOCKER_REGISTRY}."
      exit 1
      ;;
  esac
}

init_defaults
configure_docker_build
check_env

update_execution_path
build_utility_image

if [[ "${LOCAL_BUILD}" != "true" ]]; then
  publish_utility_image ".images/"
  publish_utility_image_version_tag "cached"
else
  log_this INFO "Image built locally: $(get_build_tag "${UTILITY_NAME}")"
fi
set +eu +o pipefail +E