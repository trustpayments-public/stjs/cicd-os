#!/usr/bin/env python3

import argparse
import json
import os
import string
import sys
from typing import Any, Dict, List
from pathlib import Path
import re
import yaml

# Add common cicd_libraries to path.
sys.path.append(os.path.abspath(__file__).split('/.cicd_scripts')[0] + '/.cicd_scripts')
from cicd_libraries.initialize_logging import logging


class RenderingException(Exception):
    def __init__(self, *args):
        super().__init__(*args)


class RulesetNotFoundException(RenderingException):
    def __init__(self, job_rules_var, rules_filename):
        super().__init__(f'Ruleset "{job_rules_var}" not found in {rules_filename}.')


class MalformedDisableRuleNameException(RenderingException):
    def __init__(self, name, allowed_pattern):
        super().__init__(f'DISABLE_JOB_* rule name "{name}" does not conform '
                         f'to Environment Variable name specification: "{allowed_pattern}".')


class MalformedFileException(Exception):
    def __init__(self, filename, cause):
        super().__init__(f'Malformed file "{filename}": {cause}')


class PipelineNotLockedException(Exception):
    def __init__(self, destination, unlocked_filename):
        super().__init__(
            f'The "{destination}" directory was not updated properly. File {unlocked_filename} has unlocked changes. '
            f'Use .cicd/pipeline/lock.sh to lock "{destination}" directory.'
        )


class RulesInjector(string.Formatter):

    _rules_filename: str
    _rules: Dict[str, Any]
    _env_var_name_pattern = re.compile("^[a-zA-Z_]+[a-zA-Z0-9_]*$")

    def __init__(self, rules_filename: str):
        super().__init__()

        self._rules_filename = rules_filename
        with open(rules_filename, mode='r', encoding='utf-8') as rules_file:
            self._rules = yaml.load(rules_file, Loader=yaml.FullLoader)
            logging.debug(f'Rules loaded from: {rules_filename}')

    def _check_env_var_name_pattern(self, name):
        if not self._env_var_name_pattern.match(name):
            raise MalformedDisableRuleNameException(name, self._env_var_name_pattern.pattern)

    def _render_rules(self, job_disabled_var: str, job_rules_vars: List[str]) -> str:

        job_disabled_rule = []

        if job_disabled_var:
            self._check_env_var_name_pattern(job_disabled_var)
            job_disabled_rule.append({
                'if': f'$DISABLE_JOB_{job_disabled_var} == "true"',
                'when': 'never',
            })

        job_rules = []
        for job_rules_var in job_rules_vars:
            if job_rules_var not in self._rules:
                raise RulesetNotFoundException(job_rules_var, self._rules_filename)

            job_rules.extend(self._rules.get(job_rules_var, {}).get('rules', []))

        rules_list = [
            *job_disabled_rule,
            *job_rules,
        ]

        return json.dumps(rules_list)

    def get_value(self, key, args, kwargs):
        return key

    @staticmethod
    def _render_untouched(value, spec):
        if spec:
            return f'{{{value}:{spec}}}'
        elif value:
            return f'{{{value}}}'
        else:
            return '{}'

    def format_field(self, value, spec):

        try:
            if value == '@inject-rules':
                parameters = spec.split(':')
                job_disabled_var = parameters[0]
                job_rules_vars = parameters[1:] if len(parameters) > 1 else []
                return self._render_rules(job_disabled_var, job_rules_vars)
            else:
                return self._render_untouched(value, spec)

        except RenderingException as rendering_ex:
            raise RenderingException(f'{rendering_ex} Malformed macro: "{self._render_untouched(value, spec)}".')


def list_files(path: str, file_type: str) -> List[Path]:

    matches = []
    for path in Path(path).rglob(f'*.{file_type}'):
        matches.append(path)

    logging.debug(f'{len(matches)} *.{file_type} file(s) found.')
    return matches


def needs_replacement(filename: Path, expected_contents: str) -> bool:

    if not filename.exists():
        return True

    with open(str(filename), mode='r', encoding='utf-8') as file:
        contents = file.read()

    return contents != expected_contents


def remove_orphans(source: str, destination: str):

    if not Path(destination).exists():
        return 0

    orphaned_count = 0
    destination_filenames: List[Path] = list_files(destination, "yml")
    for destination_filename in destination_filenames:
        source_filename = Path(source).joinpath(destination_filename.relative_to(destination))
        if not source_filename.exists():
            destination_filename.unlink()
            logging.info(f'Removed orphaned {destination_filename}')

    return orphaned_count


def process(source: str, destination: str, rules_file_name: str, validate_only: bool):
    rules_injector = RulesInjector(rules_file_name)
    source_filenames: List[Path] = list_files(source, "yml")

    replaced_count = 0
    for source_filename in source_filenames:
        with open(str(source_filename), mode='r', encoding='utf-8') as source_file:
            try:
                contents = rules_injector.format(source_file.read())
            except RenderingException as rendering_ex:
                raise MalformedFileException(source_filename, str(rendering_ex))

        destination_filename = Path(destination).joinpath(source_filename.relative_to(source))

        if needs_replacement(destination_filename, contents):
            if validate_only:
                raise PipelineNotLockedException(destination, source_filename)
            else:
                destination_filename.parent.mkdir(parents=True, exist_ok=True)
                with open(str(destination_filename), mode='w', encoding='utf-8') as destination_file:
                    destination_file.write(contents)

                logging.info(f'Rendered {source_filename} to {destination_filename}')
                replaced_count += 1

    return replaced_count


def main(source: str, destination: str, rules_file_name: str, validate_only: bool):

    orphaned_count = 0
    if not validate_only:
        orphaned_count = remove_orphans(source, destination)

    replaced_count = process(source, destination, rules_file_name, validate_only)

    if orphaned_count > 0:
        logging.info(f'pipeline.lock - {orphaned_count} files removed.')

    if replaced_count > 0:
        logging.info(f'pipeline.lock - {replaced_count} files locked.')

    if replaced_count == 0 and orphaned_count == 0:
        logging.info(f'pipeline.lock - all good.')


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    req_args = parser.add_argument_group('required arguments')
    req_args.add_argument('-s', '--source', required=True)
    req_args.add_argument('-d', '--destination', required=True)
    req_args.add_argument('-r', '--rules-file', required=True)
    opt_args = parser.add_argument_group('optional arguments')
    opt_args.add_argument('--validate-only', required=False, action='store_true')

    arguments = parser.parse_args()

    try:
        main(os.path.normpath(arguments.source),
             os.path.normpath(arguments.destination),
             arguments.rules_file,
             arguments.validate_only)

    except (MalformedFileException, PipelineNotLockedException) as ex:
        logging.error(str(ex))
        exit(1)
