#!/usr/bin/env bash
set -eu -o pipefail -E

DIR="${0%/*}"

# shellcheck source=../other/utils.sh
source "${DIR}/../other/utils.sh"

PROJECT_VENV_DIR="${DIR}/venv"
GITLAB_DIR="${DIR}/../../.gitlab"
PY_VERSION="${PY_VERSION:-"python"}"

function check_dependencies() {
    if [[ ! -d "${PROJECT_VENV_DIR}" ]]; then
        echo "Creating PROJECT_VENV_DIR ${PROJECT_VENV_DIR}"
        "${PY_VERSION}" -m venv "${PROJECT_VENV_DIR}"
        # shellcheck disable=SC1090
        . "${PROJECT_VENV_DIR}/bin/activate"
        "${PY_VERSION}" -m pip install --upgrade pip setuptools
        "${PY_VERSION}" -m pip install pyyaml

    else
        echo "PROJECT_VENV_DIR ${PROJECT_VENV_DIR} already exists"
        # shellcheck disable=SC1090
        . "${PROJECT_VENV_DIR}/bin/activate"
    fi
}

(
  # Running in subshell so that we dont pollute current shell with Venv Activation.
  set -eu -o pipefail -E
  check_dependencies
  "${PY_VERSION}" "${DIR}/pipeline_files_lock.py"     \
      --source "${GITLAB_DIR}/pipeline"               \
      --destination "${GITLAB_DIR}/pipeline.lock"     \
      --rules-file "${GITLAB_DIR}/pipeline/rules.yml" \
      "$@"
)