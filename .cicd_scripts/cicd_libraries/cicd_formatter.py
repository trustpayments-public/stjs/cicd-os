import logging as logging_module
import re

URL_REGEX = r"(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))"


class CICDFormatter(logging_module.Formatter):
    """Logging Formatter with colours"""

    reset = '\033[0m'
    debug = '\033[39;21m'
    info = '\033[92;21m'
    warn = '\033[93;21m'
    error = '\033[91;21m'
    critical = '\033[91;1m'
    url = '\033[96;4m'

    datefmt = '%Y-%m-%d %H:%M:%S'

    colours = {
        logging_module.DEBUG: debug,
        logging_module.INFO: info,
        logging_module.WARNING: warn,
        logging_module.ERROR: error,
        logging_module.CRITICAL: critical
    }

    def format(self, record):
        transformed_message = self._transform_all_urls(record.getMessage())
        colour = self.colours.get(record.levelno)
        log_fmt = self._get_colour_format(colour, transformed_message)
        formatter = logging_module.Formatter(fmt=log_fmt, datefmt=CICDFormatter.datefmt)
        return formatter.format(record)

    def _get_colour_format(self, colour, message: str) -> str:
        return f'%(asctime)s.%(msecs)03d {colour}%(levelname)s{self.reset} - %(funcName)s: {message}'

    def _transform_all_urls(self, message: str) -> str:
        urls = _find_url_in_message(message)
        for url in urls:
            new_url = f'{self.url}{url}{self.reset}'
            message = message.replace(url, new_url)
        return message


def _find_url_in_message(message: str):
    url = re.findall(URL_REGEX, message)
    return [x[0] for x in url]



