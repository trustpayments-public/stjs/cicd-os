import logging as logging_module
import os

from cicd_libraries.cicd_formatter import CICDFormatter


def initialize_logging() -> None:
    log_level = os.environ.get('LOG_LEVEL', 'INFO')

    logger = logging_module.getLogger("cicd")
    logger.setLevel(log_level)

    ch = logging_module.StreamHandler()
    ch.setLevel(log_level)
    ch.setFormatter(CICDFormatter())

    logger.addHandler(ch)


logging = logging_module.getLogger("cicd")
initialize_logging()
