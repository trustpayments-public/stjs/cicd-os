#!/usr/bin/env bash
source .cicd_scripts/other/utils.sh

copy_results_if_exists() {
  local source_dir=$1
  if [ -d "$source_dir" ]; then
    log_this DEBUG "Directory ${source_dir} exists"
    local new_path="${ARTIFACT_TESTS_RESULT_PATH}"/$(basename "${source_dir}")
    mkdir -p "${new_path}"
    log_this DEBUG "Copying ${source_dir} to ${new_path}"
    cp -r "${source_dir}" "${new_path}"
    log_this INFO "Copy success [${source_dir} -> ${new_path}]"
  else
    log_this INFO "Directory ${source_dir} not exists. Nothing to copy"
  fi
}

set -eu -o pipefail -E

log_debug_variable ARTIFACT_TESTS_RESULT_PATH "${ARTIFACT_TESTS_RESULT_PATH}"
mkdir -p "${ARTIFACT_TESTS_RESULT_PATH}"

copy_results_if_exists "/app/screenshots/actual"
copy_results_if_exists "/app/screenshots/results"
copy_results_if_exists "/app/reports"
