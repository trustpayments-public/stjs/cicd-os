#!/usr/bin/env bash
source .cicd_scripts/other/utils.sh

_init() {
  log_this DEBUG "pwd: $(pwd)"

  log_debug_variable WORKDIR "${WORKDIR}"
  cd "${WORKDIR}"

  source venv/bin/activate
}

execute_tests_sequentially() {
  set -eu -o pipefail -E
  log_this DEBUG "execute_tests_sequentially start"
  _init

  local behave_logging_level=$1
  local tests_dir=$2
  local feature_file=$3

  log_this INFO "Running sequential tests for ${feature_file} with ${behave_logging_level} log level"

  python3 -m behave \
    --color \
    --logging-level "${behave_logging_level}" \
    "${tests_dir}/${feature_file}"

  log_this INFO "Finished tests for ${feature_file}"

  log_this DEBUG "execute_tests_sequentially finish"
}

execute_tests_in_parallel() {
  set -eu -o pipefail -E
  log_this DEBUG "execute_tests_in_parallel start"
  _init

  local behave_logging_level=$1
  local tests_dir=$2
  local test_name=$3

  log_this INFO "Running parallel tests with ${behave_logging_level}. Actual node: ${CI_NODE_INDEX}/${CI_NODE_TOTAL}"
  FEATURES=($(ls "${tests_dir}" | grep .feature | grep "${test_name}" | grep -vE $IGNORED_FILES))

  len=${#FEATURES[@]}

  if [ $CI_NODE_TOTAL -ne $len ]; then
      log_this ERROR "Number of nodes is not equal to the number of files that should be run in parallel. Nodes: ${CI_NODE_TOTAL}. Feature files: ${len}. Update gitlab job parallel setting to number of files you want to execute in parallel. "
      exit 1
  fi

  local i=$(($CI_NODE_INDEX-1))

  log_this INFO "Executing feature file: ${FEATURES[$i]}"
  python3 -m behave \
    --color \
    --logging-level "${behave_logging_level}" \
    "${tests_dir}/${FEATURES[$i]}"

  log_this DEBUG "execute_tests_in_parallel finish"
}

execute_tests_sequentially_browserstack() {
  set -eu -o pipefail -E
  log_this DEBUG "execute_tests_sequentially_browserstack start"
  _init

  local behave_logging_level=$1
  local tests_dir=$2
  local poetry_tags=$3

  log_this INFO "Running browserstack tests for tags: ${poetry_tags} with ${behave_logging_level} log level"

  poetry run behave \
    --logging-level="${BEHAVE_LOGGING_LEVEL}" \
    --tags="${poetry_tags}" \
    "${tests_dir}"

  log_this INFO "Finished tests for tags: ${poetry_tags}"

  log_this DEBUG "execute_tests_sequentially_browserstack finish"
}

validate_and_activate_e2e_variables() {
  set -eu -o pipefail -E
  log_this DEBUG "validate_and_activate_e2e_variables start"

  test -n "${EMAIL_LOGIN:-}"
  test -n "${EMAIL_PASSWORD:-}"
  test -n "${JWT_ISS_KEY:-}"
  test -n "${JWT_SECRET_KEY_BASE64:-}"
  JWT_SECRET_KEY=$(echo ${JWT_SECRET_KEY_BASE64} | base64 -d)
  export JWT_SECRET_KEY
  test -n "${JWT_SECRET_KEY:-}"

  log_this DEBUG "validate_and_activate_e2e_variables finish"
}