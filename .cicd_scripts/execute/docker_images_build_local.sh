#!/usr/bin/env bash

# A script for building BASE / PIPELINE images locally.
# The settings in this script attempt to mimic Gitlab Environment, with following exceptions:
# - `HTTP_PROXY` / `HTTPS_PROXY` variables and their lower-case counterparts are not set to prevent the images
#    from having ProxyCommand configured in ~/.ssh/config for gitlab.com.
# - `LOCAL_BUILD` is set which turns off following functionalities:
#    - Logging in to docker registries. You need to manually log-in to all required Gitlab / DockerHub / ECR / ...
#      registries, using your private credentials.
#    - Publishing images - no docker push commands should be performed
#    - Caching - no `docker pull` commands will be performed to pre-fetch base / pipeline / other images
#      for caching layers.
# - You need to have an SSH key that allows access to all Gitlab Repositories required by Pipeline / Project
#   Docker Images. On Gitlab this is provided as `GITLAB_SSH_PRIVATE_KEY_BASE64` environment variable, and
#   you must either provide one for the script, or ensure `~/.ssh/id_rsa` file exists in your home directory
#   and that public part of that key is registered in Gitlab.
#
# WARNING!
# The images created by this script will have your Private SSH Key inside!
# Be extra careful not to publish them anywhere!
#
# To build both images locally, simply run below script in project folder:
# .cicd_scripts/execute/docker_images_build_local.sh
#
# To run selectively:
# .cicd_scripts/execute/docker_images_build_local.sh -i base
# .cicd_scripts/execute/docker_images_build_local.sh -i pipeline

set -eu -o pipefail -E

DIR="${0%/*}"

# shellcheck source=../other/utils.sh
source "${DIR}/../other/utils.sh"

function _mock_gitlab_runner() {

  log_this INFO "Enabling local build (no docker logins or pushes to remote registries)..."
  export LOCAL_BUILD="true"

  log_this INFO "Mocking gitlab runner environment variables..."

  # INFRA-572: Only mock pipeline id if it's missing. This will prevent unit tests on Gitlab
  # from accidentally overwriting their images if two jobs run on the same runner.
  export CI_PIPELINE_ID="${CI_PIPELINE_ID:-"local"}"

  # This is only required for building ECR Repository names according to used naming conventions.
  # Not really something we do when building locally.
  export CI_PROJECT_NAME="local"

  CI_COMMIT_REF_SLUG="$(git rev-parse --abbrev-ref HEAD)"
  export CI_COMMIT_REF_SLUG

  export CI_REGISTRY_USER=""
  export CI_REGISTRY_PASSWORD=""
  export CI_REGISTRY_IMAGE=""
  export CI_REGISTRY=""

  export IMAGE_PIPELINE_WORKDIR="/pipeline"

  # Prepare SSH key if not otherwise set
  if [[ "${GITLAB_SSH_PRIVATE_KEY_BASE64:-""}" == "" ]]; then
    log_this WARN "$(echo -ne "GITLAB_SSH_PRIVATE_KEY_BASE64 not specified. "
                     echo -ne "Falling back to using '~/.ssh/id_rsa' as Private Key for Gitlab Access.")"

    GITLAB_SSH_PRIVATE_KEY_BASE64="$(base64 --wrap 0 ~/.ssh/id_rsa)"
    export GITLAB_SSH_PRIVATE_KEY_BASE64
  fi

  export IMAGE_MASTER_BASE="registry.gitlab.com/trustpayments-public/public-docker-registry/base:0.0.196"
  export IMAGE_MASTER_PIPELINE="pipeline:master"

  # Note: this does not define how images will be tagged.
  # It merely tells IMAGE_PIPELINE build the name of IMAGE_BASE.
  export IMAGE_BASE="base:current-build-${CI_PIPELINE_ID}"
  export IMAGE_PIPELINE="pipeline:current-build-${CI_PIPELINE_ID}"
}

function usage() {
  echo "Usage:"
  echo "$(basename "$0") [[-i <image>], [-i image], ...]"
  echo ""
  echo "Where:"
  echo " -i <image>     Specifies image to build."
  echo "                Allowed image names are: base, pipeline."
  echo "                When ran without -i parameter, both images are built by default."
}

# Default variables
IMAGE_BASE_PIPELINE="registry.gitlab.com/trustpayments-public/public-docker-registry/base-pipeline:0.0.196"

# Parse and validate arguments
IMAGES_SPECIFIED="false"
BUILD_BASE="false"
BUILD_PIPELINE="false"
while [[ $# -gt -0 ]]; do

  case "$1" in
    -i|--image)
      IMAGES_SPECIFIED="true"
      case "$2" in
        base)
          BUILD_BASE="true"
          ;;
        pipeline)
          BUILD_PIPELINE="true"
          ;;
        *)
          (echo >&2 "Invalid image name: $2. Allowed: base, pipeline.")
          exit 1
          ;;
      esac
      shift
      ;;
    -h|--help)
      usage
      exit 0
      ;;
    *)
      (echo >&2 "Invalid argument: $1")
      usage
      exit 1
      ;;
  esac
  shift

done

if [[ "${IMAGES_SPECIFIED}" == "false" ]]; then
  BUILD_BASE="true"
  BUILD_PIPELINE="true"
fi

export BUILD_BASE
export BUILD_PIPELINE
export IMAGE_BASE_PIPELINE

_mock_gitlab_runner
"${DIR}"/docker_images_build.sh
