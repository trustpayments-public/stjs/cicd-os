#!/usr/bin/env bash
set -eu -o pipefail -E

DIR="${0%/*}"

# shellcheck source=../other/utils.sh
source "${DIR}/../other/utils.sh"

# shellcheck source=../build/common/common.sh
source "${DIR}/../build/common/common.sh"

# Registry address for Docker Hub. Non-configurable from CICD.
# Selected experimentally (index.docker.io failed on Gitlab Runners).
# shellcheck disable=SC2034
DOCKERHUB_REGISTRY="docker.io"

docker_images_build() {
  set -eu -o pipefail -E

  log_this INFO "Building base/pipeline images from current branch..."

  if [[ "${BUILD_BASE:="true"}" == "true" ]]; then
    log_this INFO "Cleaning up any remaining base image caches..."
    docker rmi "${IMAGE_MASTER_BASE}"       2>/dev/null || true
    docker rmi "${IMAGE_BASE}"              2>/dev/null || true

    log_this INFO "Pulling base image: ${IMAGE_MASTER_BASE}..."

    docker pull "${IMAGE_MASTER_BASE}"
    docker tag "${IMAGE_MASTER_BASE}" "${IMAGE_BASE}"
    if [[ "${LOCAL_BUILD:="false"}" != "true" ]]; then
      docker_login "gitlab"
      log_this INFO "Pushing ${IMAGE_MASTER_BASE} as ${IMAGE_BASE}..."
      docker push "${IMAGE_BASE}"
    fi
  fi

  if [[ "${BUILD_PIPELINE:="true"}" == "true" ]]; then

    log_this INFO "Cleaning up any remaining pipeline image caches..."
    docker rmi "${IMAGE_MASTER_PIPELINE}"   2>/dev/null || true
    docker rmi "${IMAGE_PIPELINE}"          2>/dev/null || true

    log_this INFO "Building pipeline image..."

    local pipeline_args=""
    pipeline_args="${pipeline_args} --build-arg WORKDIR=${IMAGE_PIPELINE_WORKDIR}"

    # shellcheck source=../build/utility/build_utility.sh
    UTILITY_NAME='pipeline'                                    \
    DOCKERFILE_PATH='.cicd_scripts/docker/pipeline/Dockerfile' \
    INJECT_TO_PROJECT_IMAGES='false'                           \
    USE_LOG_LEVEL_ARG='true'                                   \
    BUILD_ARGS_EXTRA="${pipeline_args}"                        \
    /bin/bash "${DIR}/../build/utility/build_utility.sh"
  fi

  log_this INFO "All done."
  set +eu +o pipefail +E
}

docker_images_build

set +eu +o pipefail +E