#!/usr/bin/env bash
set -eu -o pipefail -E

DIR="${0%/*}"

# shellcheck source=../other/utils.sh
source "${DIR}/../other/utils.sh"

# shellcheck source=../build/common/common.sh
source "${DIR}/../build/common/common.sh"

# Registry address for Docker Hub. Non-configurable from CICD.
# Selected experimentally (index.docker.io failed on Gitlab Runners).
# shellcheck disable=SC2034
DOCKERHUB_REGISTRY="docker.io"

docker_images_pull() {
  set -eu -o pipefail -E

  log_this INFO "Cleaning up any remaining image caches..."
  docker rmi "${IMAGE_MASTER_BASE}"       2>/dev/null || true
  docker rmi "${IMAGE_MASTER_PIPELINE}"   2>/dev/null || true
  docker rmi "${IMAGE_BASE}"              2>/dev/null || true
  docker rmi "${IMAGE_PIPELINE}"          2>/dev/null || true

  log_this INFO "Pulling base image: ${IMAGE_MASTER_BASE}..."
  docker pull "${IMAGE_MASTER_BASE}"

  log_this INFO "Pulling pipeline image from: ${CI_REGISTRY_IMAGE}..."
  docker_login "gitlab"
  docker pull "${IMAGE_MASTER_PIPELINE}"

  log_this INFO "Pushing ${IMAGE_MASTER_BASE} as ${IMAGE_BASE}..."
  docker tag "${IMAGE_MASTER_BASE}" "${IMAGE_BASE}"
  docker push "${IMAGE_BASE}"

  log_this INFO "Pushing ${IMAGE_MASTER_PIPELINE} as ${IMAGE_PIPELINE}..."
  docker tag "${IMAGE_MASTER_PIPELINE}" "${IMAGE_PIPELINE}"
  docker push "${IMAGE_PIPELINE}"

  log_this INFO "All done."
  set +eu +o pipefail +E
}

docker_images_pull

set +eu +o pipefail +E