import os
import sys

import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

# Add common cicd_libraries to path.
sys.path.append(os.path.abspath(__file__).split('/.cicd_scripts')[0] + '/.cicd_scripts')
from cicd_libraries.initialize_logging import logging


def get(url, gitlab_api_token, params=None, data=None, should_raise_for_status=True):
    headers = {'PRIVATE-TOKEN': gitlab_api_token}
    _log_request_data(url, params, data, should_raise_for_status)
    response = _gen_session().get(url, params=params, headers=headers, data=data)
    return _gitlab_api_response(response, should_raise_for_status)


def post(url, gitlab_api_token, params=None, data=None, should_raise_for_status=True):
    headers = {'PRIVATE-TOKEN': gitlab_api_token}
    _log_request_data(url, params, data, should_raise_for_status)
    response = _gen_session().post(url, params=params, headers=headers, data=data)
    return _gitlab_api_response(response, should_raise_for_status)


def put(url, gitlab_api_token, params=None, data=None, should_raise_for_status=True):
    headers = {'PRIVATE-TOKEN': gitlab_api_token}
    _log_request_data(url, params, data, should_raise_for_status)
    response = _gen_session().put(url, params=params, headers=headers, data=data)
    return _gitlab_api_response(response, should_raise_for_status)


def _gitlab_api_response(response, should_raise_for_status=True):
    logging.debug(f'response.text: {response.text}')
    if should_raise_for_status:
        response.raise_for_status()
    return response


def _gen_session():
    session = requests.Session()
    retry = Retry(
        connect=3,
        status=5,
        backoff_factor=0.5,
        status_forcelist=[404, 408, 500, 502, 503, 504],
        allowed_methods=["HEAD", "GET", "PUT", "POST", "OPTIONS"]
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session


def _log_request_data(url, params, data, should_raise_for_status):
    logging.debug(f'url: {url}')
    logging.debug(f'params: {params}')
    logging.debug(f'data: {data}')
    logging.debug(f'should_raise_for_status: {should_raise_for_status}')

