#!/usr/bin/env bash
# Example usage
# /bin/bash .cicd_scripts/gitlab/delete_docker_registry.sh "${GITLAB_API_TOKEN}" "${PROJECT_ID}"
source .cicd_scripts/other/utils.sh

_delete_registry_tag() {
  local gitlab_api_token=$1
  local project_id=$2
  local registry_id=$3
  local tag_id=$4

  curl -X DELETE --silent --header "PRIVATE-TOKEN: ${gitlab_api_token}" \
    "https://gitlab.com/api/v4/projects/${project_id}/registry/repositories/${registry_id}/tags/${tag_id}" > /dev/null 2>&1

  log_this INFO "Deleted. project_id: ${project_id}, registry_id: ${registry_id}, tag_id: ${tag_id}"
}

_delete_registry() {
  log_this INFO "Start deleting registry. project_id: ${project_id}, registry_id: ${registry_id}"
  local gitlab_api_token=$1
  local project_id=$2
  local registry_id=$3

  curl -X DELETE --silent --header "PRIVATE-TOKEN: ${gitlab_api_token}" \
    "https://gitlab.com/api/v4/projects/${project_id}/registry/repositories/${registry_id}" > /dev/null 2>&1

  # Delete all tags - this will speedup deleting registry

  local registry_tags=$(curl --silent --header "PRIVATE-TOKEN: ${gitlab_api_token}" \
      "https://gitlab.com/api/v4/projects/${project_id}/registry/repositories/${registry_id}/tags" | jq -r .[].name)

  while [[ "${registry_tags:=""}" != "" ]]; do

    for registry_tag_id in ${registry_tags} ; do
      log_this DEBUG "registry_tag_id ${registry_tag_id}"
      _delete_registry_tag "${gitlab_api_token}" "${project_id}" "${registry_id}" "${registry_tag_id}" &
    done
#    sleep 1 # You will hit too much request anyway, but less often

    wait # Waiting for all _delete_registry_tag executed in background

    local registry_tags=$(curl --silent --header "PRIVATE-TOKEN: ${gitlab_api_token}" \
      "https://gitlab.com/api/v4/projects/${project_id}/registry/repositories/${registry_id}/tags" | jq -r .[].name)
  done
}

delete_registries() {
  log_this INFO "delete_registries start"
  local gitlab_api_token=$1
  local project_id=$2

  local registries=$(curl --silent --header "PRIVATE-TOKEN: ${gitlab_api_token}" \
    "https://gitlab.com/api/v4/projects/${project_id}/registry/repositories" | jq -r .[].id)

  for registry_id in ${registries} ; do
    log_this DEBUG "registry_id ${registry_id}"
    _delete_registry "${gitlab_api_token}" "${project_id}" "${registry_id}"
  done

  log_this INFO "delete_registries done"
}

set -eu -o pipefail -E

GITLAB_API_TOKEN=$1
PROJECT_ID=$2

delete_registries "${GITLAB_API_TOKEN}" "${PROJECT_ID}"




