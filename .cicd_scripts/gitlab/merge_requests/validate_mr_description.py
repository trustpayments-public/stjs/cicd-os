import argparse
from re import search
import markdown
from bs4 import BeautifulSoup
from cicd_libraries.initialize_logging import logging
from gitlab import gitlab_api_request


VALIDATION_CRITERIA_DOCS_PATH = '.cicd_docs/pipeline_steps_automatic.md#validate-merge-request-description'

# Requirements for description validation
VALID_H1S = ['Changes', 'Breaking changes', 'New features', 'Hotfix', 'Fixes']

# Validation error messages
INVALID_H1S_MESSAGE = f'''
    Description format error: No valid H1s found. Please check MR validation criteria: {VALIDATION_CRITERIA_DOCS_PATH}
    '''

# Validation exception patterns for validation
CICD_VEXCEPT_PATTERN = r'^cicd-[0-9]+$'
RENOVATE_VEXCEPT_PATTERN = r'^renovate\/.*$'

def _get_command_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--gitlab-api-token', type=str, dest='gitlab_api_token', required=True)
    parser.add_argument('--gitlab-project-id', type=str, dest='gitlab_project_id', required=True)
    parser.add_argument('--ref-name', type=str, dest='ref_name', required=True)

    return parser.parse_args()


def _get_merge_requests_data(gitlab_project_id: str, gitlab_api_token: str, ref_name: str):
    url = f'https://gitlab.com/api/v4/projects/{gitlab_project_id}/merge_requests?source_branch={ref_name}&state=opened'
    response_get = gitlab_api_request.get(url, gitlab_api_token)
    merge_requests = response_get.json()
    mrs_data = []
    for merge_request in merge_requests:
        mrs_data.append({
            'web_url' : merge_request['web_url'],
            'id' : merge_request['id'],
            'description' : merge_request['description']
        })

    return mrs_data


def _validate_merge_requests_description(mr_data: dict):
    logging.info(f'Validating description for Merge Request #{mr_data["id"]}')
    logging.info(f'Web URL: {mr_data["web_url"]}')

    html = markdown.markdown(mr_data['description'])
    soup = BeautifulSoup(html, 'html.parser')
    logging.debug(f'Description (HTML): {soup.prettify()}')

    if not _validate_h1s(soup):
        raise Exception(f'{INVALID_H1S_MESSAGE}')

    logging.info('Description is valid!')


def _validate_h1s(soup):
    h1s = soup.find_all('h1')
    logging.debug(f'Found H1s: {h1s}')
    valid = False
    for valid_h1 in VALID_H1S:
        for section_header in h1s:
            if section_header.string:
                if valid_h1 in section_header.string:
                    if not section_header.nextSibling.name == 'h1':
                        valid = True
                        logging.info(f'Found valid H1: {valid_h1}')

    return valid


def _validate_merge_requests(gitlab_project_id: str, gitlab_api_token: str, ref_name: str):
    mrs_data = _get_merge_requests_data(gitlab_project_id, gitlab_api_token, ref_name)

    if not mrs_data:
        logging.info('No merge requests found. Skipping validation...')
    else:
        logging.info(f'Found Merge Requests: {mrs_data}')
        for mr_data in mrs_data:
            _validate_merge_requests_description(mr_data)
            logging.info(f'Merge Request #{mr_data["id"]} is valid!')


def _should_branch_be_validated(ref_name: str):
    if search(CICD_VEXCEPT_PATTERN, ref_name):
        logging.info('CICD branch detected - skipping validation')
        return False
    elif search(RENOVATE_VEXCEPT_PATTERN, ref_name):
        logging.info('RENOVATE branch detected - skipping validation')
        return False
    else:
        return True


def main():
    args = _get_command_args()

    logging.debug(f'gitlab_project_id: {args.gitlab_project_id}')
    logging.debug(f'gitlab_api_token: {args.gitlab_api_token}')
    logging.debug(f'ref_name: {args.ref_name}')

    if _should_branch_be_validated(args.ref_name):
        _validate_merge_requests(args.gitlab_project_id, args.gitlab_api_token, args.ref_name)


if __name__ == '__main__':
    main()
