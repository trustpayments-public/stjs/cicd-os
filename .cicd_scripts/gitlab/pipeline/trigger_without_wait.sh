#!/usr/bin/env bash
set -eu -o pipefail -E

source .cicd_scripts/other/utils.sh

trigger_pipeline_without_wait() {
  local project_id=$1
  local trigger_token=$2
  local trigger_ref=$3
  local variables_map=$4

  log_debug_variable project_id "$project_id"
  log_debug_variable trigger_token "$trigger_token"
  log_debug_variable trigger_ref "$trigger_ref"
  log_debug_variable variables_map "$variables_map"

  ENVIRONMENT='review' python3 .cicd_scripts/gitlab/pipeline/trigger.py \
  --project-id "${project_id}" \
  --trigger-token "${trigger_token}" \
  --trigger-ref "${trigger_ref}" \
  --gitlab-api-token "${GITLAB_API_TOKEN}" \
  --trigger-variables-map "${variables_map}"
}

set -eu -o pipefail -E

SHOULD_TRIGGER=$1
log_debug_variable SHOULD_TRIGGER "$SHOULD_TRIGGER"

if [[ "${SHOULD_TRIGGER}" == "true" ]]; then
  log_this INFO "Triggering pipeline"
  trigger_pipeline_without_wait "${PROJECT_ID}" "${GITLAB_TRIGGER_TOKEN}" "${TEMPLATE_BRANCH}" "${TRIGGER_VARIABLES_MAP}"
else
  log_this INFO "Not triggering pipeline"
fi

set +eu -o pipefail +E



