import os
import sys
from datetime import datetime
from functools import lru_cache
from typing import List, Optional

# Add common cicd_libraries to path.
sys.path.append(os.path.abspath(__file__).split('/.cicd_scripts')[0] + '/.cicd_scripts')
from cicd_libraries.initialize_logging import logging
from gitlab import gitlab_api_request


def _list_pipelines(project_id: str, gitlab_api_token: str, page_number: int,
                    branch_name: str = None,
                    updated_after: datetime = None,
                    status: str = None):
    url = f'https://gitlab.com/api/v4/projects/{project_id}/pipelines'
    params = {'per_page': 10, 'page': page_number}
    if branch_name:
        params['ref'] = branch_name
    if updated_after:
        params['updated_after'] = updated_after
    if status:
        params['status'] = status

    return gitlab_api_request.get(url, gitlab_api_token, params=params)


def get_pipelines_ids(project_id: str, gitlab_api_token: str,
                      branch_name: str = None,
                      updated_after: datetime = None,
                      status: str = None):
    page = 1
    r = _list_pipelines(project_id, gitlab_api_token, page, branch_name, updated_after, status, )
    total_pages = int(r.headers["X-Total-Pages"])
    logging.info(f'{status} | total_pages: {total_pages}')

    pipelines = []
    while page <= total_pages:
        logging.debug(f"Checking page {page}/{total_pages}")
        response = _list_pipelines(project_id, gitlab_api_token, page, branch_name, updated_after, status)
        data = response.json()
        for item in data:
            logging.debug(
                f'{item["id"]} | {item["ref"]} | {item["created_at"]} | {item["updated_at"]} | {item["status"]}')
            pipelines.append(item["id"])
        page += 1

    logging.info(f'{status} | pipelines: {len(pipelines)}')
    return pipelines


def get_last_pipeline_id(project_id: str, gitlab_api_token: str,
                         branch_name: str = None,
                         updated_after: datetime = None,
                         status: str = None):
    pipelines = get_pipelines_ids(project_id, gitlab_api_token, branch_name, updated_after, status)
    if not pipelines:
        raise ValueError(f'Pipeline not found. This should not happen.')

    logging.debug(f'len(versions): {len(pipelines)}')
    logging.debug(f'before_sort pipelines: {pipelines}')
    pipelines.sort(reverse=True)
    logging.debug(f'after sort pipelines: {pipelines}')

    return pipelines[0]


def get_web_url_for_pipeline(project_id: str, pipeline_id: str, gitlab_api_token: str):
    url = f'https://gitlab.com/api/v4/projects/{project_id}/pipelines/{pipeline_id}'
    response = gitlab_api_request.get(url, gitlab_api_token)
    data = response.json()
    return data["web_url"]


@lru_cache(maxsize=None)
def get_project_full_path(project_id, gitlab_api_token) -> str:
    url = f'https://gitlab.com/api/v4/projects/{project_id}'
    response = gitlab_api_request.get(url, gitlab_api_token)
    data = response.json()
    return data["path_with_namespace"]


@lru_cache(maxsize=None)
def get_ordered_pipeline_stages(project_id, pipeline_id, gitlab_api_token) -> List[str]:
    """
    This is awkward and slow way of getting the list of ordered stages through GraphQL API.
    The reason it's awkward is we need to map pipeline_id to pipeline_iid by iterating through pipelines returned
    by GraphQL (RESTful API has no endpoint to return ordered stages, nor does it return Pipeline IIDs).

    https://gitlab.com/gitlab-org/gitlab/-/issues/267562 was reported to make the whole process easier, but for now
    there seems not to be any other way.
    """

    project_full_path = get_project_full_path(project_id, gitlab_api_token)
    pipeline_iid = get_pipeline_iid(project_id, pipeline_id, gitlab_api_token)

    gitlab_graphql_url = 'https://gitlab.com/api/graphql'
    graphql_query = f''' 
        {{
            project(fullPath: "{project_full_path}") {{
                pipeline(iid: {pipeline_iid}) {{
                    stages {{ edges {{ node {{ name }} }} }}  
                }}
            }}
        }}
    '''

    response = gitlab_api_request.post(gitlab_graphql_url, gitlab_api_token,
                                       data={'query': graphql_query})

    response_json = response.json()
    logging.debug(f'GraphQL response: {response_json}')

    edges = response_json['data']['project']['pipeline']['stages']['edges']

    stages = [edge['node']['name'] for edge in edges]
    return stages


@lru_cache(maxsize=None)
def get_pipeline_iid(project_id, pipeline_id, gitlab_api_token) -> Optional[str]:
    # TODO: We may not need this at all if this is fixed:
    # https://gitlab.com/gitlab-org/gitlab/-/issues/267562

    project_full_path = get_project_full_path(project_id, gitlab_api_token)

    gitlab_graphql_url = 'https://gitlab.com/api/graphql'
    cursor: Optional[str] = None

    while True:
        pipelines_args = 'first: 100'
        if cursor:
            pipelines_args += f' after: "{cursor}"'

        graphql_query = f''' 
            {{
                project(fullPath: "{project_full_path}") {{
                    pipelines({pipelines_args}) {{
                        pageInfo {{ endCursor }}
                        nodes {{ id iid }} 
                    }}
                }}
            }}
        '''
        response = gitlab_api_request.post(gitlab_graphql_url, gitlab_api_token,
                                           data={'query': graphql_query})

        response_json = response.json()
        logging.debug(f'GraphQL response: {response_json}')

        cursor = response_json['data']['project']['pipelines']['pageInfo']['endCursor']

        pipeline_iid: Optional[str] = \
            next((node['iid'] for node in response_json['data']['project']['pipelines']['nodes']
                  if node['id'] == f'gid://gitlab/Ci::Pipeline/{pipeline_id}'), None)

        if pipeline_iid is not None:
            break

        if cursor is None:
            break

    return pipeline_iid
