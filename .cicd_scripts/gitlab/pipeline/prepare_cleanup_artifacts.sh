#!/usr/bin/env bash
set -eu -o pipefail -E

DIR="${0%/*}"

# shellcheck source=../../other/utils.sh
source "${DIR}/../../other/utils.sh"

function check_env() {
    log_info_variable ARTIFACT_ALL_SOURCES                  "${ARTIFACT_ALL_SOURCES}"
}

function prepare_all_sources() {
  log_this INFO "Exposing all project sources as artifact for cleanup/destroy jobs."
  mkdir -p "${ARTIFACT_ALL_SOURCES}"
  rsync -a ./ "${ARTIFACT_ALL_SOURCES}" --exclude '.all_sources_*'
}

check_env
prepare_all_sources