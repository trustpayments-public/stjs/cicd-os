#!/usr/bin/env bash
set -eu -o pipefail -E

source .cicd_scripts/other/utils.sh

wait_for_related_pipelines() {
  log_this INFO "Start wait_for_related_pipelines"

  : "${ENVIRONMENT:="None"}"

  log_debug_variable ENVIRONMENT "${ENVIRONMENT}"
  log_debug_variable CI_PROJECT_ID "${CI_PROJECT_ID}"
  log_debug_variable CI_PIPELINE_ID "${CI_PIPELINE_ID}"
  log_debug_variable GITLAB_API_TOKEN "${GITLAB_API_TOKEN}"
  log_debug_variable CI_COMMIT_REF_NAME "${CI_COMMIT_REF_NAME}"

  ENVIRONMENT=$ENVIRONMENT \
  python3 .cicd_scripts/gitlab/pipeline/wait_for_related_pipelines.py \
  --project-id "${CI_PROJECT_ID}" \
  --pipeline-id "${CI_PIPELINE_ID}" \
  --gitlab-api-token "${GITLAB_API_TOKEN}" \
  --ref "${CI_COMMIT_REF_NAME}"

  log_this INFO "Finish wait_for_related_pipelines"
}