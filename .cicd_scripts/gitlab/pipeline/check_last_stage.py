#!/usr/bin/env python3
#
# Checks if all previous jobs has been finished successfully.
# Sample execution:
# python3 ./scripts/python/gitlab/pipeline/check_last_stage.py

import os
import sys

import common_status

# Add common cicd_libraries to path.
sys.path.append(os.path.abspath(__file__).split('/.cicd_scripts')[0] + '/.cicd_scripts')
from cicd_libraries.initialize_logging import logging

CI_PROJECT_ID = os.environ['CI_PROJECT_ID']
CI_PIPELINE_ID = os.environ['CI_PIPELINE_ID']
CI_JOB_STAGE = os.environ['CI_JOB_STAGE']
GITLAB_API_TOKEN = os.environ['GITLAB_API_TOKEN']
SUCCESS_STATUSES = ['success']
ALLOWED_FOR_STATUS_MANUAL = []
ALLOWED_PREFIXES_TO_IGNORE = ()
INTRO_TEXT = "Checking if all previous stages succeed"


class GitLabCheck(Exception):
    def __init__(self, project_id, pipeline_id, job_stage, failed_status):
        super().__init__()
        self._project_id = project_id
        self._pipeline_id = pipeline_id
        self._job_stage = job_stage
        self._failed_status = failed_status

    def __str__(self):
        return f'GitLabCheck: ProjectID: {self._project_id}, PipelineID: {self._pipeline_id}, ' \
               f'JobStage: {self._job_stage}, FailedStatus: {self._failed_status}'


def main():
    logging.info(f'{INTRO_TEXT}')

    logging.debug(f'CI_PROJECT_ID: {CI_PROJECT_ID}')
    logging.debug(f'CI_PIPELINE_ID: {CI_PIPELINE_ID}')
    logging.debug(f'CI_JOB_STAGE: {CI_JOB_STAGE}')
    logging.debug(f'GITLAB_API_TOKEN: {GITLAB_API_TOKEN}')

    statuses = common_status.get_last_statuses_for_previous_stage(CI_PROJECT_ID, CI_PIPELINE_ID, GITLAB_API_TOKEN,
                                                                  CI_JOB_STAGE)

    for item in statuses:
        if item.name.lower().startswith(ALLOWED_PREFIXES_TO_IGNORE):
            logging.info(f'PASS [ignored status]: {item} ')
        elif item.status in SUCCESS_STATUSES:
            logging.info(f'PASS: {item}')
        elif item.status == 'manual' and item.name.lower() in ALLOWED_FOR_STATUS_MANUAL:
            logging.info(f'PASS [allowed manual]: {item} ')
        else:
            raise GitLabCheck(CI_PROJECT_ID, CI_PIPELINE_ID, CI_JOB_STAGE, item)

    logging.info(f'Success')


if __name__ == '__main__':
    main()
