#!/usr/bin/env python3
#
# Waiting till other pipelines finish their jobs.
#
# Sample execution: python3 .cicd_scripts/gitlab/pipeline/wait_for_related_pipelines.py
import argparse
import os
import sys

from common_wait import EnvNotAllowed, find_related_pipelines, validate_actual_pipeline_exists, get_pipelines_to_wait_for, wait_for_jobs_in_pipeline

# Add common cicd_libraries to path.
sys.path.append(os.path.abspath(__file__).split('/.cicd_scripts')[0] + '/.cicd_scripts')
from cicd_libraries.initialize_logging import logging

ALLOWED_ENVIRONMENTS = ('review', 'prestage', 'stage-eu', 'prod-eu')


def get_command_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--project-id', type=str, dest='project_id', required=True)
    parser.add_argument('--pipeline-id', type=str, dest='pipeline_id', required=True)
    parser.add_argument('--gitlab-api-token', type=str, dest='gitlab_api_token', required=True)
    parser.add_argument('--ref', type=str, dest='ref', required=True)
    parser.add_argument('--search-filter-last-hours', type=int, dest='search_filter_last_hours', required=False,
                        default=24)

    return parser.parse_args()


def main():
    args = get_command_args()
    logging.debug(f'project_id: {args.project_id}')
    logging.debug(f'pipeline_id: {args.pipeline_id}')
    logging.debug(f'gitlab_api_token: {args.gitlab_api_token}')
    logging.debug(f'ref: {args.ref}')
    logging.debug(f'search_filter_last_hours: {args.search_filter_last_hours}')
    logging.info(f'Waiting till other pipelines finish their jobs')
    
    env = os.environ['ENVIRONMENT']

    if env == "None":
        actual_env = None
    else:
        actual_env = env if args.ref == 'master' else 'review'
        logging.info(f'Checking {actual_env} for {args.ref}')
        if actual_env not in ALLOWED_ENVIRONMENTS:
            raise EnvNotAllowed(actual_env, ALLOWED_ENVIRONMENTS)
    logging.info(f'Actual env: {actual_env}')
    
    related_pipelines = find_related_pipelines(args.project_id, args.gitlab_api_token, args.ref,
                                               args.search_filter_last_hours)

    actual_pipeline = int(args.pipeline_id)
    validate_actual_pipeline_exists(actual_pipeline, related_pipelines)

    pipelines_to_wait_for = get_pipelines_to_wait_for(actual_pipeline, related_pipelines)

    if pipelines_to_wait_for:
        logging.info(f'There are {len(pipelines_to_wait_for)} pipelines that I should check: {pipelines_to_wait_for}')
        for pipeline_id in pipelines_to_wait_for:
            wait_for_jobs_in_pipeline(args.project_id, pipeline_id, args.gitlab_api_token, actual_env)
    else:
        logging.info(f'There is no pipeline that I should wait for.')

    logging.info(f'Success')


if __name__ == '__main__':
    main()
