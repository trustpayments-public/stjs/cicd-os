import json
import os
import sys
import time
from typing import Iterable, Optional
from datetime import datetime, timedelta
import common_status
from common_status import GitLabJobStatus
import common_find
from typing import List

# Add common cicd_libraries to path.
sys.path.append(os.path.abspath(__file__).split('/.cicd_scripts')[0] + '/.cicd_scripts')
from cicd_libraries.initialize_logging import logging
from gitlab import gitlab_api_request


CHECK_IGNORE_GITLAB_STAGES = ['initialize', 'pre-build', 'build', 'post-build', 'publish-to-cdn', 'release',
                              'scheduled-smoke-tests', 'scheduled-smoke-tests-report',
                              'validate-merge-request-description']
EXPLICIT_WAIT_FOR_STEPS = ['build-core-infrastructure']
ALLOWED_GITLAB_STAGE_PREFIXES = ('review', 'prestage', 'stage', 'prod')
# ALL statuses : running, pending, success, failed, canceled, skipped, created, manual
STATUSES_FOR_WAIT = ['running', 'pending', 'created', 'waiting_for_resource']


class PipelineFail(Exception):
    def __init__(self, pipeline_id: str, web_url: str):
        self._pipeline_id = pipeline_id
        self._web_url = web_url

    def __str__(self):
        return f'Pipeline {self._pipeline_id} failed. Check details: {self._web_url}'


class PipelineUnknownStatus(Exception):
    def __init__(self, pipeline_id: str, web_url: str, status: str):
        self._pipeline_id = pipeline_id
        self._web_url = web_url
        self._status = status

    def __str__(self):
        return f'Pipeline {self._pipeline_id} have status {self._status}. Check details: {self._web_url}'


class PipelineNotFoundException(Exception):
    def __init__(self, pipeline: str):
        self._pipeline = pipeline

    def __str__(self):
        return f'Pipeline {self._pipeline} not found in pipelines'


class GitLabStageNameException(Exception):
    def __init__(self, stage: str):
        self._stage = stage

    def __str__(self):
        return f'Stage {self._stage} is not allowed. This is a bug. Contact developers.'


class EnvNotAllowed(Exception):
    def __init__(self, env: str, allowed_envs: Iterable[str]):
        self._env = env
        self._allowed_envs = allowed_envs

    def __str__(self):
        return f'{self._env} not in allowed environment names: {self._allowed_envs}'


def validate_actual_pipeline_exists(actual_pipeline, pipelines):
    if actual_pipeline not in pipelines:
        logging.info(f'pipelines: {pipelines}')
        raise PipelineNotFoundException(actual_pipeline)
    logging.info(f'Validation success. Actual pipeline exists.')


def find_related_pipelines(project_id, gitlab_api_token, branch_name, filter_count_last_hours: int):
    updated_after = datetime.now() - timedelta(hours=filter_count_last_hours)

    pipelines = []
    statuses_to_search = STATUSES_FOR_WAIT
    for status in statuses_to_search:
        related_pipelines = common_find.get_pipelines_ids(project_id, gitlab_api_token, branch_name, updated_after,
                                                          status)
        pipelines.extend(related_pipelines)
    return pipelines


def get_pipelines_to_wait_for(actual_pipeline, pipelines):
    return sorted(i for i in pipelines if i < actual_pipeline)


def _get_job_status(project_id, job_id, gitlab_api_token):
    gitlab_job_url = f'https://gitlab.com/api/v4/projects/{project_id}/jobs/{job_id}'
    response = gitlab_api_request.get(gitlab_job_url, gitlab_api_token)
    data = response.json()
    logging.debug(f'{data["id"]} | {data["ref"]} | {data["created_at"]} | {data["status"]}')
    return data["status"]


def _get_data_about_unique_jobs_in_pipelines(project_id, pipeline_id, gitlab_api_token) -> List[GitLabJobStatus]:
    statuses = common_status.get_all_statuses(project_id, pipeline_id, gitlab_api_token)
    statuses = common_status.filter_by_last_unique_step_name(statuses)
    return reversed(statuses)


# review -> review | prestage -> prestage | stage-eu -> stage | stage-us -> stage-us..
def _get_env_short(env: str) -> str:
    if '-' in env:
        return env.split('-')[0]
    else:
        return env


def _should_omit_job(job: GitLabJobStatus, env):
    stage = job.stage.lower()
    job_name = job.name.lower()

    if job_name in EXPLICIT_WAIT_FOR_STEPS:
        job.log_info(f'Job is in EXPLICIT_WAIT_FOR_STEPS. Should wait.')
        return False

    if stage in CHECK_IGNORE_GITLAB_STAGES:
        job.log_info(f'Stage in in CHECK_IGNORE_GITLAB_STAGES. Omitting.')
        return True

    if not stage.startswith(ALLOWED_GITLAB_STAGE_PREFIXES):
        job.log_error(f'Stage {stage} is not allowed. ALLOWED_GITLAB_STAGE_PREFIXES = {ALLOWED_GITLAB_STAGE_PREFIXES}')
        raise GitLabStageNameException(stage)

    short_env = _get_env_short(env)
    if not stage.startswith(short_env):
        job.log_info(f'Stage {stage} is not env: {short_env}. Omitting.')
        return True

    return False


def wait_for_jobs_in_pipeline(project_id, pipeline_id, gitlab_api_token, env: Optional[str] = None,
                              sleep_time_in_seconds: int = 10):
    web_url = common_find.get_web_url_for_pipeline(project_id, pipeline_id, gitlab_api_token)
    logging.info(f'Wait for pipeline {pipeline_id} in project {project_id} using GitLab API')
    logging.info(f'Start | web_url: {web_url}')

    # Omitting depends from env. If env is not set, then Omitting is off.
    if env:
        log_str = f'Checking if pipeline {pipeline_id} has finished jobs for env {env}'
    else:
        log_str = f'Checking if pipeline {pipeline_id} has finished all jobs. Omitting is off.'

    should_retry = True
    while should_retry:
        logging.info(log_str)
        should_retry = False
        job_statuses = _get_data_about_unique_jobs_in_pipelines(project_id, pipeline_id, gitlab_api_token)

        for job in job_statuses:
            logging.debug(f'{job}')
            status = job.status.lower()

            if env and _should_omit_job(job, env):
                continue

            if status in STATUSES_FOR_WAIT:
                job.log_info('Waiting for finish.')
                while status in STATUSES_FOR_WAIT:
                    time.sleep(sleep_time_in_seconds)
                    job.log_info('Still waiting')
                    status = _get_job_status(project_id, job.job_id, gitlab_api_token)
                job.log_info(f'Waiting finished. New status is {status}.')
                job.log_info(f'New data could come. Need to retry and get new data.')
                should_retry = True
                break
            else:
                job.log_info('No need to wait.')
    logging.info(f'Finish | web_url: {web_url}')


def _get_pipeline_status(project_id: str, pipeline_id: str, gitlab_api_token: str):
    gitlab_job_url = f'https://gitlab.com/api/v4/projects/{project_id}/pipelines/{pipeline_id}'
    response = gitlab_api_request.get(gitlab_job_url, gitlab_api_token)
    json_data = json.loads(response.text)
    return json_data["status"]


def wait_for_pipeline_finish(project_id: str, pipeline_id: str, gitlab_api_token: str, sleep_time_in_seconds: int = 10):
    web_url = common_find.get_web_url_for_pipeline(project_id, pipeline_id, gitlab_api_token)
    wait_for_jobs_in_pipeline(project_id, pipeline_id, gitlab_api_token, env=None)

    # ALL statuses : running, pending, success, failed, canceled, skipped, created, manual
    statuses_for_success = ['success', 'manual']
    statuses_for_fail = ['failed', 'canceled', 'skipped']
    status = _get_pipeline_status(project_id, pipeline_id, gitlab_api_token)

    while True:
        status = _get_pipeline_status(project_id, pipeline_id, gitlab_api_token)
        logging.info(f'Pipeline status: {status}')
        if status in statuses_for_success:
            return  # Everything is ok
        elif status in statuses_for_fail:
            raise PipelineFail(pipeline_id, web_url)
        elif status in STATUSES_FOR_WAIT:
            logging.info(f'Pipeline not finished sleep: {sleep_time_in_seconds}s')
            time.sleep(sleep_time_in_seconds)
        else:
            raise PipelineUnknownStatus(pipeline_id, web_url, status)

