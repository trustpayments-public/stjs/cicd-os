#!/usr/bin/env bash
set -eu -o pipefail -E

DIR="${0%/*}"

# shellcheck source=../../other/utils.sh
source "${DIR}/../../other/utils.sh"

function check_env() {
    log_info_variable CI_PROJECT_ID                         "${CI_PROJECT_ID}"
    log_info_variable CI_PIPELINE_ID                        "${CI_PIPELINE_ID}"
    log_info_variable GITLAB_API_TOKEN                      "${GITLAB_API_TOKEN}"
}

function print_variables() {
  local variables
  variables="$(
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" \
      "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/variables" |
      jq -r '.[] | [.key, .value] | @tsv'
  )"

  log_this INFO "Listing pipeline variables"
  if [[ "$(echo -n "${variables}" | wc -l)" == "0" ]]; then
    log_this INFO "Pipeline ${CI_PIPELINE_ID} has been started with no variables passed to it."
  else
    while IFS= read -r line; do
      log_info_variable "$(echo "${line}" | cut -f 1)" "$(echo "${line}" | cut -f 2)"
    done <<< "${variables}"
  fi

}

check_env
print_variables