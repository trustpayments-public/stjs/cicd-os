#!/usr/bin/env python3
import argparse
import os
import sys

import common_wait
import common_find


# Add common cicd_libraries to path.
sys.path.append(os.path.abspath(__file__).split('/.cicd_scripts')[0] + '/.cicd_scripts')
from cicd_libraries.initialize_logging import logging


def _get_command_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--project-id', type=str, dest='project_id', required=True)
    parser.add_argument('--gitlab-api-token', type=str, dest='gitlab_api_token', required=True)
    parser.add_argument('--ref', type=str, dest='ref', required=True)

    return parser.parse_args()


def main():
    args = _get_command_args()
    logging.debug(f'project_id: {args.project_id}')
    logging.debug(f'gitlab_api_token: {args.gitlab_api_token}')
    logging.debug(f'ref: {args.ref}')

    pipeline_id = common_find.get_last_pipeline_id(args.project_id, args.gitlab_api_token,
                                                   args.ref)

    common_wait.wait_for_pipeline_finish(args.project_id, pipeline_id, args.gitlab_api_token)

    logging.info(f'Success')


if __name__ == '__main__':
    main()
