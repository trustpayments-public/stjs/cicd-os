#!/usr/bin/env python3
import argparse
import os
import json
import sys
from typing import Dict, Any
import common_wait
import yaml

# Add common cicd_libraries to path.
sys.path.append(os.path.abspath(__file__).split('/.cicd_scripts')[0] + '/.cicd_scripts')
from cicd_libraries.initialize_logging import logging
from gitlab import gitlab_api_request


def get_command_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--project-id', type=str, dest='project_id', required=True)
    parser.add_argument('--gitlab-api-token', type=str, dest='gitlab_api_token', required=True)
    parser.add_argument('--trigger-token', type=str, dest='trigger_token', required=True)
    parser.add_argument('--trigger-ref', type=str, dest='trigger_ref', required=True)
    parser.add_argument('--trigger-variables-map', type=str, dest='trigger_variables_map', required=False, default='{}')
    parser.add_argument('--pass-variables-from-env-prefixes', nargs='*', dest='pass_variables_from_env_prefixes',
                        help='Scan all env variables with following prefix and add them as variables map')
    parser.add_argument('--wait', action='store_true')

    return parser.parse_args()


def trigger_pipeline(project_id: str, token: str, ref: str, variables: Dict[str, Any]):
    url = f'https://gitlab.com/api/v4/projects/{project_id}/trigger/pipeline'
    data = {
        'token': token,
        'ref': ref
    }
    for key, value in variables.items():
        data[f'variables[{key}]'] = value

    response = gitlab_api_request.post(url, token, data=data)
    json_data = json.loads(response.text)
    triggered_pipeline_id = json_data["id"]
    logging.info(f'Pipeline {triggered_pipeline_id}')
    return triggered_pipeline_id


def get_variables_from_env(prefixes):
    result = {}
    for prefix in prefixes:
        logging.info(f'Searching for env variables with prefix: {prefix}')
        for k, v in os.environ.items():
            if k.startswith(prefix):
                result[k] = v

    logging.debug(f'result: {result}')
    return result


def main():
    args = get_command_args()
    logging.debug(f'project_id: {args.project_id}')
    logging.debug(f'gitlab_api_token: {args.gitlab_api_token}')
    logging.debug(f'trigger_token: {args.trigger_token}')
    logging.debug(f'trigger_ref: {args.trigger_ref}')
    logging.debug(f'trigger_variables_map: {args.trigger_variables_map}')
    logging.debug(f'pass_variables_from_env_prefixes: {args.pass_variables_from_env_prefixes}')
    logging.debug(f'wait: {args.wait}')

    logging.info(f'Triggering pipeline for project {args.project_id} using GitLab API')

    trigger_variables_map = yaml.load(args.trigger_variables_map, Loader=yaml.FullLoader)

    if args.pass_variables_from_env_prefixes:
        variables_from_env = get_variables_from_env(args.pass_variables_from_env_prefixes)
        trigger_variables_map.update(variables_from_env)

    logging.debug(f'trigger_variables_map: {str(trigger_variables_map)}')

    pipeline_id = trigger_pipeline(args.project_id,
                                   args.trigger_token,
                                   args.trigger_ref,
                                   trigger_variables_map)

    if args.wait:
        common_wait.wait_for_pipeline_finish(args.project_id, pipeline_id, args.gitlab_api_token)

    logging.info(f'Success')


if __name__ == '__main__':
    main()
