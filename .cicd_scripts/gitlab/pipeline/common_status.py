import json
import os
import sys

from dataclasses import dataclass
from typing import List, Optional

# Add common cicd_libraries to path.
sys.path.append(os.path.abspath(__file__).split('/.cicd_scripts')[0] + '/.cicd_scripts')
from cicd_libraries.initialize_logging import logging
from gitlab import gitlab_api_request
from gitlab.pipeline.common_find import get_ordered_pipeline_stages


class PreviousStageNotFound(Exception):
    def __init__(self, stage_name, pipeline_id):
        super().__init__()
        self._stage_name = stage_name
        self._pipeline_id = pipeline_id

    def __str__(self):
        return f'PreviousStageNotFound: {self._stage_name} in pipeline {self._pipeline_id}'


@dataclass(frozen=True)
class GitLabJobStatus:
    job_id: str
    stage: str
    name: str
    created_at: str
    status: str

    @staticmethod
    def short_string(string, max_chars, dots='..'):
        return string[:max_chars - len(dots)] + (string[max_chars:] and dots)

    def _log(self, extra_message) -> str:
        max_chars_long = 30
        max_chars_short = 9
        stage = self.short_string(self.stage, max_chars_long)
        name = self.short_string(self.name, max_chars_long)
        status = self.short_string(self.status, max_chars_short)

        return f'{stage: <{max_chars_long}}' \
               f' | {self.job_id}' \
               f' | {name: <{max_chars_long}}' \
               f' | {status: <{max_chars_short}}' \
               f' | {extra_message}'

    def log_info(self, extra_message) -> None:
        logging.info(self._log(extra_message))

    def log_error(self, extra_message) -> None:
        logging.error(self._log(extra_message))


def _get_previous_stage(all_statuses: List[GitLabJobStatus], actual_stage_name, pipeline_id):
    last_stage = ""
    for status in all_statuses:
        if status.stage.lower() == actual_stage_name.lower():
            return last_stage
        last_stage = status.stage

    if last_stage == "":
        raise PreviousStageNotFound(actual_stage_name, pipeline_id)


def _filter_by_stage_name(job_statuses: List[GitLabJobStatus], stage_name) -> List[GitLabJobStatus]:
    result = []
    for item in job_statuses:
        logging.debug(f'{item.stage}')
        if item.stage.lower() == stage_name.lower():
            result.append(item)
    return result


def filter_by_last_unique_step_name(job_statuses: List[GitLabJobStatus]) -> List[GitLabJobStatus]:
    result = []
    step_names = {}
    for item in reversed(job_statuses):
        step_name = item.name
        if step_name not in step_names:
            step_names[step_name] = True
            result.append(item)
    return result


def get_last_statuses_for_previous_stage(project_id, pipeline_id, gitlab_api_token, actual_stage_name) ->\
        List[GitLabJobStatus]:
    job_status_list = get_all_statuses(project_id, pipeline_id, gitlab_api_token)
    previous_stage_name = _get_previous_stage(job_status_list, actual_stage_name, pipeline_id)
    logging.debug(f'previous_stage_name: {previous_stage_name}')

    job_status_list = _filter_by_stage_name(job_status_list, previous_stage_name)
    job_status_list = filter_by_last_unique_step_name(job_status_list)

    logging.debug(f'Filtered statuses for stage {previous_stage_name}:')
    for job_status in job_status_list:
        logging.debug(f'{job_status}')

    return job_status_list


def get_all_statuses(project_id, pipeline_id, gitlab_api_token) -> List[GitLabJobStatus]:
    page = 1
    r = _get_jobs_page_from_gitlab(project_id, pipeline_id, gitlab_api_token, page)
    total_pages = int(r.headers["X-Total-Pages"])

    job_statuses = []
    while page <= total_pages:
        logging.debug(f"Checking page {page}/{total_pages}")
        response = _get_jobs_page_from_gitlab(project_id, pipeline_id, gitlab_api_token, page)
        data = response.json()
        for item in data:
            logging.debug(f'{item["id"]} | {item["stage"]} | {item["name"]} | {item["created_at"]} | {item["status"]}')
            job_status = GitLabJobStatus(
                job_id=item["id"],
                stage=item["stage"],
                name=item["name"],
                created_at=item["created_at"],
                status=item["status"]
            )
            job_statuses.append(job_status)
        page += 1

    # Make sure jobs are sorted by stage and their ids.
    ordered_stages = get_ordered_pipeline_stages(project_id, pipeline_id, gitlab_api_token)
    return sorted(job_statuses,
                  key=lambda status: (ordered_stages.index(status.stage), int(status.job_id)))


def _get_jobs_page_from_gitlab(project_id, pipeline_id, gitlab_api_token, page_number):
    gitlab_job_url = f'https://gitlab.com/api/v4/projects/{project_id}/pipelines/{pipeline_id}/jobs'
    params = {
        'per_page': 10,
        'page': page_number
    }
    return gitlab_api_request.get(gitlab_job_url, gitlab_api_token, params)
