#!/usr/bin/env bash
source .cicd_scripts/gitlab/release/lib.sh

set -eu -o pipefail -E

log_this INFO "Generating new version."

git fetch -q --tags
git checkout -q "${CI_COMMIT_REF_NAME}"

generate_new_version