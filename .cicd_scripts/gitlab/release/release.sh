#!/usr/bin/env bash

DIR="${0%/*}"

source .cicd_scripts/gitlab/release/lib.sh
source .cicd_scripts/gitlab/release/lib_lerna.sh
source "${DIR}/../pipeline/wait_for_related_pipelines.sh"

set -eu -o pipefail -E

log_this INFO "Releasing new version."

source .cicd_scripts/gitlab/release/load_release_artifact.sh "${ARTIFACT_RELEASE_CICD_FILENAME}"

# Here we are waiting for related pipeline just in case previous pipeline still not created release (unlikely, but possible)
# This is to avoid issues with overlapping commit history in release changes
wait_for_related_pipelines
create_release

log_this INFO "Success"
