import argparse
import json
from datetime import datetime

from cicd_libraries.initialize_logging import logging
from gitlab import gitlab_api_request
from gitlab.release.release_note import ReleaseNote
from gitlab.release.lib import find_last_release_tag
from pytz import timezone

TIMEZONE = 'Europe/London'


def _get_options():
    parser = argparse.ArgumentParser(description='Create new release in gitlab')
    parser.add_argument('--gitlab-api-token', type=str, dest='gitlab_api_token', required=True)
    parser.add_argument('--gitlab-project-id', type=str, dest='gitlab_project_id', required=True)
    parser.add_argument('--git-ref', type=str, dest='git_ref', required=True)
    parser.add_argument('--release-data-file', type=str, dest='release_data_file', required=True)
    parser.add_argument('--description-type', type=str, dest='description_type', required=True)
    args = parser.parse_args()

    with open(args.release_data_file) as json_file:
        data = json.load(json_file)

    logging.debug(f'data: {data}')
    return {
        # from args
        'gitlab_api_token': args.gitlab_api_token,
        'gitlab_project_id': args.gitlab_project_id,
        'git_ref': args.git_ref,
        'description_type': args.description_type,
        # from data
        'release_tag': data['release_tag'],
        'release_description': data['release_description'],
    }


def _gen_release_time(region):
    time_zone = timezone(region)
    release_time = datetime.now(tz=time_zone)

    return release_time


def _create_or_update_resource(gitlab_project_id: str, gitlab_api_token: str, data):
    logging.info('Start')
    url = f'https://gitlab.com/api/v4/projects/{gitlab_project_id}/releases/'
    response_post = gitlab_api_request.post(
        url, gitlab_api_token, data=data, should_raise_for_status=False
    )
    if response_post.status_code == 409:
        del data['description']
        logging.info(f'Got {response_post.status_code}, sending PUT request')
        put_url = f'{url}/{data["tag_name"]}'
        gitlab_api_request.put(put_url, gitlab_api_token, data=data)
    elif response_post.status_code != 201:
        raise ValueError(response_post.text)
    logging.info('Success')


def _generate_description(
    gitlab_project_id: str, gitlab_api_token: str, description_type: str, git_ref: str
):
    last_full_release_tag = find_last_release_tag(
        gitlab_project_id, gitlab_api_token, include_upcoming=False
    )
    logging.info(f'last_full_release_tag: {last_full_release_tag}')

    return ReleaseNote(
        gitlab_project_id, gitlab_api_token, last_full_release_tag, git_ref
    ).print_description(description_type)


def _create_gitlab_release(
    gitlab_project_id: str,
    gitlab_api_token: str,
    release_version: str,
    release_description: str,
    description_type: str,
    git_ref: str,
    region: str,
):
    logging.info(f'Creating gitlab release with version {release_version}')
    release_at = _gen_release_time(region=region)

    if release_description == 'to_be_populated':
        release_description = _generate_description(
            gitlab_project_id, gitlab_api_token, description_type, git_ref
        )
    logging.info(f'release_description: {release_description}')

    git_tag = release_version

    data = {
        'name': release_version,
        'tag_name': git_tag,
        'ref': git_ref,
        'description': release_description,
        'released_at': release_at.isoformat(),
    }
    logging.debug(f'data: {data}')
    _create_or_update_resource(gitlab_project_id, gitlab_api_token, data)


def main():
    options = _get_options()

    logging.debug(f'gitlab_project_id: {options["gitlab_project_id"]}')
    logging.debug(f'gitlab_api_token: {options["gitlab_api_token"]}')
    logging.debug(f'release_tag: {options["release_tag"]}')
    logging.debug(f'release_description: {options["release_description"]}')
    logging.debug(f'git_ref: {options["git_ref"]}')

    _create_gitlab_release(
        gitlab_project_id=options['gitlab_project_id'],
        gitlab_api_token=options['gitlab_api_token'],
        release_version=options['release_tag'],
        release_description=options['release_description'],
        description_type=options['description_type'],
        git_ref=options['git_ref'],
        region=TIMEZONE,
    )


if __name__ == '__main__':
    main()
