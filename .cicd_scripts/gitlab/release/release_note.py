from re import search

import git
from cicd_libraries.initialize_logging import logging
from gitlab import gitlab_api_request


class ReleaseNote:
    def __init__(
        self, gitlab_project_id: str, gitlab_api_token: str, last_release_ref: str, git_ref: str
    ):
        self._gitlab_project_id = gitlab_project_id
        self._gitlab_api_token = gitlab_api_token
        self._last_release_ref = last_release_ref
        self._git_ref = git_ref

    def _get_merge_requests(self, git_refs: list):
        parsed_mrs = []
        url = f'https://gitlab.com/api/v4/projects/{self._gitlab_project_id}/merge_requests?state=merged'
        response_get = gitlab_api_request.get(url, self._gitlab_api_token)
        while True:
            merge_requests = response_get.json()
            for merge_request in merge_requests:
                logging.debug(f'Found MR: {merge_request}')
                if merge_request['merge_commit_sha'] in git_refs:
                    parsed_mr = {
                        'title': merge_request['title'],
                        'web_url': merge_request['web_url'],
                    }
                    if not search(r'^renovate\/.*$', merge_request['source_branch']):
                        parsed_mr['description'] = merge_request['description']
                    parsed_mrs.append(parsed_mr)
            if 'next' in response_get.links.keys():
                next_page = response_get.links['next']['url']
                response_get = gitlab_api_request.get(next_page, self._gitlab_api_token)
            else:
                break
        return parsed_mrs

    def _print_from_merge_requests(self):
        git_refs = self._get_refs_list_from_range()
        merge_requests = self._get_merge_requests(git_refs)
        description = ''
        # Get last MR with full description
        if not merge_requests:
            return ''
        for merge_request in merge_requests:
            description += f'# {merge_request["web_url"]} - {merge_request["title"]}\n'
            if 'description' in merge_request.keys():
                description += '> ' + merge_request['description'].replace('\n', '\n> ') + '\n'
        return description + '\n'

    def print_description(self, description_type: str):
        description = ''
        if description_type.lower() == 'EXTENDED'.lower():
            description += self._print_from_merge_requests()
        description += self._print_commits_history()
        return description

    def _print_commits_history(self):
        commit_history = '# Commit history \n'
        if self._git_ref == self._last_release_ref:
            commit_history += 'No changes since previous release.'
        else:
            git_manager = git.Git(working_dir='.')
            logs = git_manager.log(
                '--oneline', '--no-merges', f'{self._last_release_ref}..{self._git_ref}'
            ).split('\n')
            log_md = [f'- {log}' for log in logs]
            commit_history += '\n'.join(log_md)
        return commit_history

    def _get_refs_list_from_range(self):
        git_manager = git.Git(working_dir='.')
        refs = git_manager.log(
            '--format=%H', '--merges', f'{self._last_release_ref}..{self._git_ref}'
        ).split('\n')
        logging.info(f'Refs in scope: {refs}')
        return refs
