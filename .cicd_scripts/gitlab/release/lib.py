import semantic_version
from cicd_libraries.initialize_logging import logging
from gitlab import gitlab_api_request
from gitlab.release.exceptions import LastReleaseNotExistsException


def find_last_release_tag(
    gitlab_project_id: str, gitlab_api_token: str, include_upcoming: bool = True
) -> str:
    logging.debug(f'gitlab_project_id: {gitlab_project_id}')
    tags = _get_all_tags(gitlab_project_id, gitlab_api_token)
    if len(tags) == 0:
        raise LastReleaseNotExistsException(gitlab_project_id)

    versions = _filter_all_semantic_versions(tags, include_upcoming)
    logging.debug(f'len(versions): {len(versions)}')
    logging.debug(f'before_sort versions: {versions}')
    versions.sort(reverse=True)
    logging.debug(f'after sort versions: {versions}')

    latest_version = str(versions[0])
    if latest_version in tags:
        return latest_version
    elif f'upcoming-{latest_version}' in tags and include_upcoming:
        return f'upcoming-{latest_version}'
    else:
        raise Exception('Release version not found!')


def _filter_all_semantic_versions(tags: list, include_upcoming: bool = True) -> list:
    versions = []
    for tag in tags:
        try:
            if tag.startswith('upcoming-') and not include_upcoming:
                logging.debug(f'Ignoring upcoming tag: {tag}')
                continue
            ver = semantic_version.Version(tag.replace('upcoming-', ''))
            logging.debug(f'Valid semantic version: {ver}')
            versions.append(ver)
        except ValueError:
            logging.debug(f'Invalid semantic version: {tag}')
    return versions


def _get_all_tags(gitlab_project_id: str, gitlab_api_token: str):
    page = 1
    response = _get_releases_tags_page(gitlab_project_id, gitlab_api_token, page)
    total_pages = int(response.headers['X-Total-Pages'])
    tags = []
    while page <= total_pages:
        logging.debug(f'Checking page {page}/{total_pages}')
        response = _get_releases_tags_page(gitlab_project_id, gitlab_api_token, page).json()
        for tag in response:
            tag_name = tag['name']
            logging.debug(f'tag_name: {tag_name}')
            tags.append(tag_name)
        page += 1
    return tags


def _get_releases_tags_page(gitlab_project_id: str, gitlab_api_token: str, page: int):
    url = f'https://gitlab.com/api/v4/projects/{gitlab_project_id}/repository/tags'
    params = {'order_by': 'name', 'per_page': 10, 'page': page}
    return gitlab_api_request.get(url, gitlab_api_token, params=params)
