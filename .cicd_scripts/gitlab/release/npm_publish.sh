#!/usr/bin/env bash
source .cicd_scripts/gitlab/release/lib.sh
source .cicd_scripts/gitlab/release/lib_lerna.sh

set -eu -o pipefail -E

log_this INFO "Publishing new version."

validate_dir_exists "${ARTIFACT_RELEASE_SOURCES}"
cd "${ARTIFACT_RELEASE_SOURCES}"

npm_lerna_check_versions
npm_lerna_publish

log_this INFO "Success"
