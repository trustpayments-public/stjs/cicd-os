class LastReleaseNotExistsException(Exception):
    def __init__(self, project_id):
        super().__init__()
        self._project_id = project_id

    def __str__(self):
        return f'LastReleaseNotExistsException: ProjectID: {self._project_id}'
