#!/usr/bin/env bash
source .cicd_scripts/other/utils.sh

function _get_value_from_json() {
  set -eu -o pipefail -E
  local filename=$1
  local key=$2
  log_debug_variable filename "${filename}"
  log_debug_variable key "${key}"

  local value=$(cat "${filename}" | jq "${key}" | sed "s/\"//g")
  log_debug_variable value "${value}"

  echo "${value}"
}

ARTIFACT_FILENAME=$1
log_debug_variable ARTIFACT_FILENAME "${ARTIFACT_FILENAME}"
validate_file_exists "${ARTIFACT_FILENAME}"

export RELEASE_TAG=$(_get_value_from_json "${ARTIFACT_FILENAME}" '.release_tag')
export RELEASE_DESCRIPTION=$(_get_value_from_json "${ARTIFACT_FILENAME}" '.release_description')
export UPCOMING_RELEASE_TAG=$(_get_value_from_json "${ARTIFACT_FILENAME}" '.upcoming_release_tag')
export BRANCH_FOR_NEW_TAG=$(_get_value_from_json "${ARTIFACT_FILENAME}" '.branch_for_new_tag')
export CDN_HASH=$(_get_value_from_json "${ARTIFACT_FILENAME}" '.cdn_hash')
