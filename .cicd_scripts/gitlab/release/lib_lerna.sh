#!/usr/bin/env bash
source .cicd_scripts/other/utils.sh


npm_lerna_check_versions() {
  log_this INFO "Start npm_lerna_check_versions"

  log_this INFO "node version: $(node --version)"
  log_this INFO "npm version:  $(npm --version)"
  log_this INFO "npx version:  $(npx --version)"
  log_this INFO "lerna version:  $(npx lerna --version)"

  log_this INFO "Finish npm_lerna_check_versions"
}

npm_lerna_publish() {
  log_this INFO "Start npm_lerna_publish"

  echo "//registry.npmjs.org/:_authToken=\${NPM_TOKEN}" >> $HOME/.npmrc 2> /dev/null
  npx lerna publish from-package --no-git-tag-version --no-push --yes

  log_this INFO "Finish npm_lerna_publish"
}
