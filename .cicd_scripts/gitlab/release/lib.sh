#!/usr/bin/env bash
source .cicd_scripts/other/utils.sh

generate_new_version() {
  log_this INFO "Generating new version."

  : "${VERSION_PART_TO_UPDATE:="PATCH"}"
  : "${CDN_HASH_KEY:="${CI_PIPELINE_ID}"}"

  log_debug_variable GITLAB_API_TOKEN "${GITLAB_API_TOKEN}"
  log_debug_variable CI_PROJECT_ID "${CI_PROJECT_ID}"
  log_debug_variable GIT_REF "${GIT_REF}"
  log_debug_variable VERSION_PART_TO_UPDATE "${VERSION_PART_TO_UPDATE}"
  log_debug_variable ARTIFACT_RELEASE_CICD_FILENAME "${ARTIFACT_RELEASE_CICD_FILENAME}"
  log_debug_variable CDN_HASH_KEY "${CDN_HASH_KEY}"
  log_debug_variable PWD "$(pwd)"

  python3 .cicd_scripts/gitlab/release/generate_new_release_data.py \
  --gitlab-api-token "${GITLAB_API_TOKEN}" \
  --gitlab-project-id "${CI_PROJECT_ID}" \
  --git-ref "${GIT_REF}" \
  --update-part "${VERSION_PART_TO_UPDATE}" \
  --cdn-hash-key "${CDN_HASH_KEY}" \
  --file-name "${ARTIFACT_RELEASE_CICD_FILENAME}"
}

create_release() {
  log_this INFO "Creating release"

  : "${ARTIFACT_RELEASE_DESCRIPTION_TYPE:="BASIC"}"

  log_debug_variable GITLAB_API_TOKEN "${GITLAB_API_TOKEN}"
  log_debug_variable GITLAB_PROJECT_ID "${GITLAB_PROJECT_ID}"
  log_debug_variable GIT_REF "${GIT_REF}"
  log_debug_variable ARTIFACT_RELEASE_DESCRIPTION_TYPE "${ARTIFACT_RELEASE_DESCRIPTION_TYPE}"
  log_debug_variable ARTIFACT_RELEASE_CICD_FILENAME "${ARTIFACT_RELEASE_CICD_FILENAME}"

  set +u

  python3 .cicd_scripts/gitlab/release/create.py \
    --gitlab-api-token "${GITLAB_API_TOKEN}" \
    --gitlab-project-id "${GITLAB_PROJECT_ID}" \
    --git-ref "${GIT_REF}" \
    --description-type "${ARTIFACT_RELEASE_DESCRIPTION_TYPE}" \
    --release-data-file "${ARTIFACT_RELEASE_CICD_FILENAME}"
}
