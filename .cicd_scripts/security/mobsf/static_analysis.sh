#!/usr/bin/env bash
set -eu -o pipefail -E

# A script for running security static analysis using MobSF tool
# it can be used scan (APK/XAPK/IPA/ZIP/APPX) files
# - `MOBSF_PDF_REPORT` file name for output PDF report
# - `MOBSF_JSON_REPORT` file name for output PDF report
# - `MOBSF_SECURITY_MIN_SCORE` minimal score result for scan
# Note: before run this script, MobSF need to be already running under url: http://mobsf:8000

# Example usage
# /bin/bash .cicd_scripts/security/mobsf/static_analysis.sh path/to/file.apk
source .cicd_scripts/other/utils.sh

mobsf_static_analysis() {
  local scan_file=$1
  log_debug_variable MOBSF_API_KEY "$MOBSF_API_KEY"

  log_this INFO "MobSF scan file: ${scan_file}"

  log_this DEBUG "PDF report: ${MOBSF_PDF_REPORT}"
  log_this DEBUG "JSON report: ${MOBSF_JSON_REPORT}"

  log_this INFO "Upload artifacts: ${scan_file} to MobSF"
  local response=$(curl -F "file=@${scan_file}" http://mobsf:8000/api/v1/upload -H "Authorization:${MOBSF_API_KEY}")
  log_this INFO "Response: ${response}"

  local scan_type=$(echo $response | jq '.scan_type' | sed 's/"//g')
  local file_name=$(echo $response | jq '.file_name' | sed 's/"//g')
  local hash=$(echo $response | jq '.hash' | sed 's/"//g')

  log_this INFO "Run testing: ${file_name} Scan type: ${scan_type} Hash: ${hash} "
  response=$(curl -X POST --url http://mobsf:8000/api/v1/scan --data "scan_type=${scan_type}&file_name=${file_name}&hash=${hash}" -H "Authorization:${MOBSF_API_KEY}")

  log_this INFO "Save MobSF json report for: ${hash} in: ${MOBSF_JSON_REPORT}"
  echo "${response}" >"${MOBSF_JSON_REPORT}"

  log_this INFO "Save MobSF PDF report for: ${hash} in: ${MOBSF_PDF_REPORT}"
  curl -X POST --url http://mobsf:8000/api/v1/download_pdf --data "hash=${hash}" -H "Authorization:${MOBSF_API_KEY}" --output "${MOBSF_PDF_REPORT}"

  log_this INFO "Validate security score result with min score: ${MOBSF_SECURITY_MIN_SCORE}"
  security_score_result=$(echo $response | jq '.security_score' | sed 's/"//g')

  if [[ $security_score_result -lt $MOBSF_SECURITY_MIN_SCORE ]]; then
    log_this ERROR "Failed: security score result: ${security_score_result} below min score: ${MOBSF_SECURITY_MIN_SCORE}"
    exit 1
  fi

  log_this INFO "Passed: security score result: ${security_score_result} min score: ${MOBSF_SECURITY_MIN_SCORE}"
}

mobsf_static_analysis "$1"
