import argparse
import glob
import os
import sys
import xml.etree.ElementTree as ET

# Add common cicd_libraries to path.
sys.path.append(os.path.abspath(__file__).split('/.cicd_scripts')[0] + '/.cicd_scripts')
from cicd_libraries.initialize_logging import logging


def get_command_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--source-dir', type=str, required=True)
    parser.add_argument('--site-domain', type=str, required=True)
    parser.add_argument('--destination-path', type=str, default='.')

    return parser.parse_args()


def get_file_paths(source_dir):
    logging.debug(f'Getting js and html files from {source_dir}')
    files = []
    files.extend(glob.glob(f"{source_dir}/*.js"))
    files.extend(glob.glob(f"{source_dir}/*.html"))

    return [file_path.split(source_dir)[1] for file_path in files]


def create_sitemap_xml(file_paths, site_domain):
    logging.debug('Creating xml sitemap...')
    root = ET.Element("urlset")
    root.attrib['xmlns'] = "http://www.sitemaps.org/schemas/sitemap/0.9"

    for file_path in file_paths:
        url = ET.SubElement(root, "url")
        loc = ET.SubElement(url, "loc")
        loc.text = f"{site_domain}/{file_path}"

    return root


def write_sitemap_xml(xml_root, destination_path):
    logging.info(f'Writing sitemap.xml to {destination_path}')
    tree = ET.ElementTree(xml_root)
    tree.write(f'{destination_path}/sitemap.xml', xml_declaration=True)


def main():
    args = get_command_args()
    files_paths = get_file_paths(args.source_dir)
    logging.debug(f'Found files: {files_paths}')
    sitemap_xml = create_sitemap_xml(files_paths, args.site_domain)
    write_sitemap_xml(sitemap_xml, args.destination_path)


if __name__ == "__main__":
    main()
