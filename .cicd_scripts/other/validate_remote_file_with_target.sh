#!/usr/bin/env bash
set -eu -o pipefail -E

# A script for validating remote file with target file
# it can be used to check if file in CDN is updated
# - `SLEEP_BEFORE_VALIDATE` time after files should match
# - `REMOTE_FILE_URL` url to remote file
# - `TARGET_FILE_PATH` target local file path

# Example usage
# /bin/bash .cicd_scripts/other/validate_remote_file_with_target.sh
source .cicd_scripts/other/utils.sh

function _log_env() {
    log_info_variable SLEEP_BEFORE_VALIDATE             "${SLEEP_BEFORE_VALIDATE}"
    log_info_variable REMOTE_FILE_URL                   "${REMOTE_FILE_URL}"
    log_info_variable TARGET_FILE_PATH                  "${TARGET_FILE_PATH}"
}

function _sleep_before_validate() {
  local sleep_time=$1
  log_this INFO "Wait ${sleep_time}s before validate files"
  sleep "${sleep_time}"
}

function _validate_remote_file_match_with_target() {
  local remote_url=$1
  local file_path=$2

  log_this INFO "Validate file: ${remote_url} with target file: ${file_path}"

  remote_resource_md5sum=$(curl -s "${remote_url}" | md5sum | cut -d ' ' -f 1)
  log_this INFO "Remote md5sum: ${remote_resource_md5sum}"

  local_resource_md5sum=$(md5sum "${file_path}" | cut -d ' ' -f 1)
  log_this INFO "Expected md5sum: ${local_resource_md5sum}"

  if [[ "$local_resource_md5sum" == "$remote_resource_md5sum" ]]; then
    log_this INFO "Remote file: ${remote_url} match with local file: ${file_path}"
  else
    log_this ERROR "Remote file: ${remote_url} not match with: ${file_path}"
    exit 1
  fi

}

set -eu -o pipefail -E

_log_env
_sleep_before_validate "${SLEEP_BEFORE_VALIDATE}"
_validate_remote_file_match_with_target "${REMOTE_FILE_URL}" "${TARGET_FILE_PATH}"
