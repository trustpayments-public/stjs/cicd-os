#!/usr/bin/env bash
source .cicd_scripts/other/utils.sh

download_keystore() {
  set -u -o pipefail -E

  local temp_keystore_file=$1

  log_this INFO "Downloading keystore ..."
  echo "${KEY_STORE_BASE64}" | base64 -d >"${temp_keystore_file}"
  log_this INFO "Keystore saved"
}

verify_signed_jar() {
  set -u -o pipefail -E

  local file_path=$1

  log_this INFO "Verify jar ${file_path} is signed"
  signature="$(jarsigner -verify -verbose -certs "${file_path}")"
  log_this DEBUG "${signature}"

  if echo "${signature}" | grep -q 'jar is unsigned'; then
    log_this ERROR "JAR ${file_path} is unsigned"
    exit 1
  fi

  log_this INFO "JAR ${file_path} is signed"
}

sign_jar() {
  set -u -o pipefail -E

  local temp_signed_file="./temp-signed-file.jar"
  local temp_keystore_file="./temp-keystore.jks"

  download_keystore "${temp_keystore_file}"

  log_this INFO "Signing: ${SOURCE_FILE}..."
  jarsigner -keystore "${temp_keystore_file}" \
    -storepass "${STORE_PASS}" \
    -keypass "${KEY_PASS}" \
    -signedjar "${temp_signed_file}" \
    -verbose "${SOURCE_FILE}" "${KEY_NAME}"

  log_this INFO "Cleaning: ..."
  rm "${temp_keystore_file}"
  rm "${SOURCE_FILE}"
  mv "${temp_signed_file}" "${SIGNED_FILE}"

  verify_signed_jar "${SIGNED_FILE}"

  log_this INFO "Signing: ${SOURCE_FILE} ended successfully signed JAR: ${SIGNED_FILE}"
}
