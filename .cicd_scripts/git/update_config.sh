#!/usr/bin/env bash

update_git_config_origin_to_use_ssh() {
  local origin_url=$(git remote get-url origin)
  log_debug_variable origin_url "${origin_url}"

  local new_origin_url="git@gitlab.com:${CI_PROJECT_PATH}.git"
  log_debug_variable new_origin_url "${new_origin_url}"

  git remote set-url origin "${new_origin_url}"
}
