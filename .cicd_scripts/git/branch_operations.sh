#!/usr/bin/env bash
source .cicd_scripts/other/utils.sh

_delete_cached_dir_if_exists() {
  log_this INFO "Start _delete_cached_dir_if_exists"

  local path=$1
  log_this INFO "Check if repo was cached: ${path}"
  if [ -d "$path" ]; then
    log_this INFO "Cached directory exists. Deleting"
    rm -rf "${path}"
    log_this INFO "Cached directory deleted"
  fi

  log_this INFO "Finish _delete_cached_dir_if_exists"
}

_clone_and_checkout() {
  log_this INFO "Start _update_cicd_ref"

  local project_namespace=$1
  local project_name=$(basename "${project_namespace}")
  local branch=$2
  local create_if_missing=${3:-"false"}

  log_debug_variable project_namespace "${project_namespace}"
  log_debug_variable project_name "${project_name}"
  log_debug_variable branch "${branch}"

  _delete_cached_dir_if_exists "${project_name}"
  log_this INFO "Cloning ${project_namespace}"
  git clone "git@gitlab.com:${project_namespace}.git" "${project_name}"
  cd "${project_name}"
  log_debug_variable create_if_missing "${create_if_missing}"

  if [[ "$create_if_missing" == "true" ]]; then
    log_this INFO "Checkout ${branch} (create if missing)"
    git checkout "${branch}" >/dev/null 2>&1 || git checkout -b "${branch}"
  else
    log_this INFO "Checkout ${branch} (don't create if missing)"
    git checkout "${branch}"
  fi

  log_this INFO "Finish _update_cicd_ref"
}

_update_cicd_ref() {
  log_this INFO "Start _update_cicd_ref"

  local gitlab_file=".gitlab-ci.yml"
  local cicd_ref=$1
  local template_branch_name=$2
  local push_type=$3

  log_debug_variable cicd_ref "${cicd_ref}"
  log_debug_variable template_branch_name "${template_branch_name}"
  log_debug_variable push_type "${push_type}"

  local old_first_line=$(head -1 ${gitlab_file})
  log_debug_variable old_first_line "$old_first_line"
  local new_first_line="$(head -1 ${gitlab_file} | cut -d' ' -f1,2) ${cicd_ref}"
  log_debug_variable new_first_line "$new_first_line"

  if [[ "$old_first_line" == "$new_first_line" ]]; then
    log_this INFO "CICD version has not changed. No need to commit."
    export SHOULD_TRIGGER="true"
  else
    log_this INFO "CICD version needs to be updated."

    log_this DEBUG "Before update: $(head -1 ${gitlab_file})"
    local tmp_file=".gitlab-ci.yml.tmp"
    echo "$new_first_line" > "$tmp_file"
    tail -n +2 "$gitlab_file" >> "$tmp_file"
    mv "$tmp_file" "$gitlab_file"
    log_this DEBUG "After update: $(head -1 ${gitlab_file})"

    git add "${gitlab_file}"
    log_this INFO "Changes added. Committing..."
    git commit -m "Updated CICD version to ${cicd_ref}"
    log_this INFO "Changes committed. Pushing..."
    if [[ "$push_type" == "new_upstream_branch" ]]; then
      log_this INFO "Creating new upstream branch ${template_branch_name}"
      git push --set-upstream origin "${template_branch_name}" -o ci.variable="BUILD_IMAGES=${BUILD_IMAGES:-false}"
    elif [[ "$push_type" == "skip_ci" ]]; then
      log_this INFO "Pushing to existing branch without triggering CICD pipeline"
      git push -o ci.skip
    else
      log_this ERROR "Invalid push type! Valid types are 'new_upstream_branch' or 'skip_ci'"
      exit 1
    fi
    log_this INFO "New version has been pushed to repository."

    local commit_sha=$(git rev-parse HEAD)
    log_this INFO "Commit SHA: ${commit_sha}"

    export SHOULD_TRIGGER="false"
  fi

  log_this INFO "Finish _update_cicd_ref"
}

merge_to_master() {
  set -eu -o pipefail -E
  log_this INFO "Start merge_to_master"

  local project_namespace=$1
  local branch=$2
  local new_cicd_ref=$3

  _clone_and_checkout "${project_namespace}" "${branch}"
  _update_cicd_ref "${new_cicd_ref}" "${branch}" "skip_ci"

  log_this INFO "Checkout master"
  git checkout master

  log_this INFO "Merging ${branch} to master"
  git merge "${branch}"
  log_this INFO "Push master"
  git push

  log_this INFO "Finish merge_to_master"
  set +eu -o pipefail +E
}

delete_remote_branch() {
  set -eu -o pipefail -E
  log_this INFO "Start delete_remote_branch"

  local project_namespace=$1
  local branch=$2

  _clone_and_checkout "${project_namespace}" "${branch}"

  log_this INFO "Checkout master"
  git checkout master

  log_this INFO "Deleting ${branch}"
  git push origin --delete "${branch}"

  log_this INFO "Finish delete_remote_branch"
  set +eu -o pipefail +E
}

###
# Successful execution of this function result status 0 and exported variable SHOULD_TRIGGER with value:
# 1. "false" - There were a git push, so pipeline has been triggered. No need to do it again.
# 2. "true" - There were no git push, so pipeline have to be triggered in other way,
git_prepare_repository() {
  set -eu -o pipefail -E
  log_this INFO "Start git_prepare_repository"

  local project_namespace=$1
  local cicd_ref=$2
  local template_branch_name=$3
  local original_path=$(pwd)

  log_debug_variable project_namespace "${project_namespace}"
  log_debug_variable cicd_ref "${cicd_ref}"
  log_debug_variable template_branch_name "${template_branch_name}"
  log_debug_variable original_path "${original_path}"

  _clone_and_checkout "${project_namespace}" "${template_branch_name}" "true"

  _update_cicd_ref "${cicd_ref}" "${template_branch_name}" "new_upstream_branch"

  cd "${original_path}"

  log_this INFO "Finish git_prepare_repository"
  set +eu -o pipefail +E
}

