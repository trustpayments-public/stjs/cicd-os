#!/usr/bin/env bash
set -eu -o pipefail -E

source .cicd_scripts/other/utils.sh
log_this INFO "Set GitLab SSH private key"

SSH_KEY_PATH="/root/gitlabkey"
SSH_CONFIG_PATH="/etc/ssh/ssh_config"

echo "$GITLAB_SSH_PRIVATE_KEY_BASE64" | base64 -d > "${SSH_KEY_PATH}"
chmod 400 "${SSH_KEY_PATH}"

cat >> ${SSH_CONFIG_PATH} <<EOL
Host gitlab.com
  Hostname altssh.gitlab.com
  User git
  Port 443
  Preferredauthentications publickey
  IdentityFile ${SSH_KEY_PATH}
EOL

validate_file_exists_and_not_empty "${SSH_KEY_PATH}"

log_this INFO "Testing SSH connect to GitLab"
ssh -o StrictHostKeyChecking=no -T gitlab.com