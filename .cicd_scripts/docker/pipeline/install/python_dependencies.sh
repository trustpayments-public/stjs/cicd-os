#!/usr/bin/env bash
set -eu -o pipefail -E

DIR="${0%/*}"

# shellcheck source=../../../other/utils.sh
source "${DIR}/../../../other/utils.sh"

log_this INFO "Installing python dependencies"

PY_VERSION=$1
PROJECT_VENV_DIR=$2
TOML_PATH=$3

"${PY_VERSION}" -m venv "${PROJECT_VENV_DIR}"

# shellcheck disable=SC1090
. "${PROJECT_VENV_DIR}/bin/activate"
"${PY_VERSION}" -m pip install --upgrade pip setuptools

if [[ -f "${TOML_PATH}" ]]; then
  /bin/bash "${DIR}/install_dev.sh"
fi

log_this INFO "Installing python packages for CICD (if not installed yet)"

"${PY_VERSION}" -m pip install \
  bandit \
  beautifulsoup4 \
  behave \
  gitpython \
  markdown \
  mypy \
  pylint \
  pylint-quotes \
  pytest \
  pytest-cov \
  pytest-depends \
  pytest_mock \
  pytest-shell \
  pytz \
  pyyaml \
  requests \
  safety \
  semantic_version
