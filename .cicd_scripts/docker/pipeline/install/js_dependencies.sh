#!/usr/bin/env bash
set -eu -o pipefail -E

DIR="${0%/*}"

# shellcheck source=../../../other/utils.sh
source "${DIR}/../../../other/utils.sh"

NODE_VERSION=12
NPM_VERSION=""

function _get_npm_version() {
  local package_json_file=$1

  NPM_VERSION=$(cat "${package_json_file}" | jq ".engines.npm" | sed "s/\"//g")

  log_this INFO "Node version: ${NODE_VERSION}"
  log_this INFO "NPM version:  ${NPM_VERSION}"

  if [[ "${NODE_VERSION}" == "null" ]] || [[ "${NPM_VERSION}" == "null" ]]; then
    log_this ERROR "Values for Node or NPM version not set"
    exit 1
  fi
}

function _install_node() {
  log_this INFO "Installing node"

  curl -sL "https://deb.nodesource.com/setup_${NODE_VERSION}.x" | bash -
  apt-get install -y nodejs

  log_this INFO "Node version: $(node -v)"
}

function _install_npm_and_packages() {
  log_this INFO "Installing npm"
  npm rebuild node-sass
  npm config set unsafe-perm true

  npm install -g npm@"${NPM_VERSION}"
  log_this INFO "NPM version:  $(npm -v)"

  log_this INFO "Installing npm packages"
  npm ci
}

function _install_js_dependencies() {
  local package_json=$1
  log_this INFO "Install JavaScript dependencies from: ${package_json}"

  if [[ -f "${package_json}" ]]; then
    _get_npm_version "${package_json}"

    _install_node
    _install_npm_and_packages
  else
    log_this INFO "File (${package_json}) not found. Not installing node, npm and npm packages"
  fi
}

_install_js_dependencies $1

set +eu +o pipefail +E
