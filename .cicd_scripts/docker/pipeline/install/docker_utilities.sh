#!/usr/bin/env bash
set -eu -o pipefail -E

DIR="${0%/*}"

# shellcheck source=../../../other/utils.sh
source "${DIR}/../../../other/utils.sh"

HADOLINT_VERSION="v1.19.0"

log_this INFO "Installing Haskell Dockerfile Linter in version ${HADOLINT_VERSION}"
HADOLINT_DOWNLOAD_URL="https://github.com/hadolint/hadolint/releases/download/${HADOLINT_VERSION}/hadolint-Linux-x86_64"
log_debug_variable HADOLINT_DOWNLOAD_URL "${HADOLINT_DOWNLOAD_URL}"
curl -L "${HADOLINT_DOWNLOAD_URL}" -o hadolint --fail
mv hadolint /usr/bin/hadolint
chmod +x /usr/bin/hadolint
