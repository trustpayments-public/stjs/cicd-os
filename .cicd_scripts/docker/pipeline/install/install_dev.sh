#!/usr/bin/env bash
set -ou pipefail -E

DIR="${0%/*}"

# shellcheck source=../../../other/utils.sh
source "${DIR}/../../../other/utils.sh"

log_this INFO "Installing python dev packages"

SERVICES=$(find ./src/* -maxdepth 0 -type d -exec basename {} \;)
SERVICES_EXTRAS=""

log_this DEBUG "SERVICES: #${SERVICES}#"

if [ -z "$SERVICES" ]; then
  log_this INFO "No services found. Nothing extra to install."
else
  mapfile -t SERVICES_LIST <<<"$SERVICES"
  echo "#SERVICES_LIST[@]: #${#SERVICES_LIST[@]}#"

  log_this INFO "Creating extras for services: #${SERVICES_LIST[*]}#"

  for service in "${SERVICES_LIST[@]}"; do
    SERVICES_EXTRAS="$SERVICES_EXTRAS --extras $service"
  done

  log_this INFO "Created extras: $SERVICES_EXTRAS"
fi

log_this INFO "Installing dev dependencies"
poetry --version

# shellcheck disable=SC2086
poetry install $SERVICES_EXTRAS
