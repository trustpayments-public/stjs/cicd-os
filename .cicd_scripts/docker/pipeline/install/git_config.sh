#!/usr/bin/env bash
set -eu -o pipefail -E

DIR="${0%/*}"

# shellcheck source=../../../other/utils.sh
source "${DIR}/../../../other/utils.sh"

log_this INFO "Configuring git"

git config --global user.email "st@gitlab.com"
git config --global user.name "ST GitLab"