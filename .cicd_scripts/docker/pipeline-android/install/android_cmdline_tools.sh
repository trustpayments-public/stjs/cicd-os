#!/usr/bin/env bash
set -o pipefail -E

# shellcheck source=../../../other/utils.sh
source "/.cicd_scripts/other/utils.sh"

# https://services.gradle.org/distributions/
ANDROID_SDK_ROOT="/opt/android-sdk"

install_android_cmdline_tools() {
  log_this INFO "Install Android cmdline tools version: ${ANDROID_CMDLINE_TOOLS_VERSION} ..."

  mkdir -p "${ANDROID_SDK_ROOT}/cmdline-tools"
  wget -q "https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_CMDLINE_TOOLS_VERSION}_latest.zip"
  unzip *tools*linux*.zip -d "${ANDROID_SDK_ROOT}/cmdline-tools"
  rm *tools*linux*.zip
  /bin/bash /install/license_accepted.sh $ANDROID_SDK_ROOT

  log_this INFO "Android cmdline tools installed"
}

check_sdk_manager_version() {
  log_this INFO "sdkmanager --version"
  sdkmanager --version
}

if [ -z "${ANDROID_CMDLINE_TOOLS_VERSION}" ]; then
  log_this INFO "Skipping Android cmdline tools installation, version not set"
else
  install_android_cmdline_tools
  check_sdk_manager_version
fi