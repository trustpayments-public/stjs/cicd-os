#!/usr/bin/env bash
set -o pipefail -E

# shellcheck source=../../../other/utils.sh
source "/.cicd_scripts/other/utils.sh"

# https://github.com/JetBrains/kotlin/releases/latest

install_kotlin() {
  log_this INFO "Install Kotlin version: ${KOTLIN_VERSION} ..."

  cd $(dirname "${KOTLIN_HOME}")
  wget -q "https://github.com/JetBrains/kotlin/releases/download/v${KOTLIN_VERSION}/kotlin-compiler-${KOTLIN_VERSION}.zip"
  unzip *kotlin*.zip
  rm *kotlin*.zip

  log_this INFO "Kotlin version installed"
}

check_kotlin_version() {
  log_this INFO "kotlin -version"
  kotlinc -version
}

if [ -z "${KOTLIN_VERSION}" ]; then
  log_this INFO "Skipping Kotlin installation, version not set"
else
  install_kotlin
  check_kotlin_version
fi