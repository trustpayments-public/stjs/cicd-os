#!/usr/bin/env bash
set -o pipefail -E

# shellcheck source=../../../other/utils.sh
source "/.cicd_scripts/other/utils.sh"

# https://services.gradle.org/distributions/
GRADLE_DIST=bin

install_gradle() {
  log_this INFO "Install Gradle version: ${GRADLE_VERSION} ..."

  cd $(dirname "${GRADLE_HOME}")
  wget -q https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-${GRADLE_DIST}.zip
  unzip gradle*.zip
  ls -d */ | sed 's/\/*$//g' | xargs -I{} mv {} gradle &&
  rm gradle*.zip

  log_this INFO "Gradle version installed"
}

check_gradle_version() {
  log_this INFO "gradle --version"
  gradle --version
}

if [ -z "${GRADLE_VERSION}" ]; then
  log_this INFO "Skipping Gradle installation, version not set"
else
  install_gradle
  check_gradle_version
fi
