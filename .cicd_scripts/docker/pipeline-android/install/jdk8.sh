#!/usr/bin/env bash
set -ou pipefail -E

# shellcheck source=../../../other/utils.sh
source "/.cicd_scripts/other/utils.sh"

install_jdk_8() {
  log_this INFO "Install JAVA JDK 8 ..."
  apt-get install -y openjdk-8-jdk ant
  rm -rf /var/cache/oracle-jdk8-installer
  log_this INFO "JAVA JDK 8 version installed"
}

update_java_certificates() {
  # Fix certificate issues, found as of https://bugs.launchpad.net/ubuntu/+source/ca-certificates-java/+bug/983302
  apt-get install -y ca-certificates-java
  update-ca-certificates -f
}

check_java_version() {
  log_this INFO "java -version"
  java -version
}

install_jdk_8
check_java_version
#update_java_certificates # TODO check if needed