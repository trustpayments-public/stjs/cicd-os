#!/usr/bin/env bash

set -u -o pipefail -E
source .cicd_scripts/other/utils.sh


DOCKER_LINT_CONFIG_FILE_DEFAULT=".cicd_scripts/static_analysis/config/.hadolint.yml"
DOCKER_LINT_SCAN_PATH_DEFAULT="docker"


lint_dockerfiles() {
  local has_errors=0
  dockerfiles=$(find "${DOCKER_LINT_SCAN_PATH}" -name "Dockerfile")
  log_this INFO "Found dockerfiles: ${dockerfiles}"
  while read -r dockerfile
  do
    log_this INFO "Scanning Dockerfile: ${dockerfile}"
    if ! hadolint --config "${DOCKER_LINT_CONFIG_FILE}" "${dockerfile}"; then has_errors=1; fi
  done <<< "$dockerfiles"
  if [ $has_errors -ne 0 ]; then exit 1; fi
}


log_this INFO "Beginning Haskell Docker Linter scan ..."
log_debug_variable DOCKER_LINT_SCAN_PATH "${DOCKER_LINT_SCAN_PATH:=$DOCKER_LINT_SCAN_PATH_DEFAULT}"
log_debug_variable DOCKER_LINT_CONFIG_FILE "${DOCKER_LINT_CONFIG_FILE:=$DOCKER_LINT_CONFIG_FILE_DEFAULT}"

lint_dockerfiles