#!/usr/bin/env bash
set -e -o pipefail -E

source .cicd_scripts/other/utils.sh

SRC_DIRS="${PROJECT_PYTHON_SOURCES}"
TEST_DIRS="${PROJECT_PYTHON_TESTS}"

log_this INFO "PROJECT_PYTHON_SOURCES: \n ${PROJECT_PYTHON_SOURCES}"
log_this INFO "PROJECT_PYTHON_TESTS: \n ${PROJECT_PYTHON_TESTS}"

if [[ -d "scripts/python" ]]; then
    SCRIPTS=$(find scripts/python -type f -iname "*.py")
    log_this DEBUG "SCRIPT FILES: ${SCRIPTS}"
fi

if [[ -d "src" ]]; then
    SRC_DIRS="${SRC_DIRS} src"
    log_this DEBUG "SRC DIRS: ${SRC_DIRS}"
fi

if [[ "${SRC_DIRS}" != "" ]]; then

    log_this INFO "Static Analysis SOURCE/SCRIPTS: (pylint)"
    pylint --rcfile=.cicd_scripts/static_analysis/config/pylintrc ${SRC_DIRS} ${SCRIPTS}

    log_this INFO "Static Analysis SOURCE/SCRIPTS: (bandit)"
    bandit -c .cicd_scripts/static_analysis/config/bandit.config --format txt --recursive ${SRC_DIRS} ${SCRIPTS}

else
    log_this INFO "Static Analysis SOURCE/SCRIPTS: Nothing to check."
fi


if [[ -d "tests" ]]; then
    TEST_DIRS="${TEST_DIRS} tests"
    for test_dir in ${TEST_DIRS}; do
      TEST_FILES="${TEST_FILES} $(find "$test_dir" -iname "*.py")"
    done
    log_this DEBUG "TEST FILES: ${TEST_FILES}"
fi

if [[ "${TEST_FILES}" != "" ]]; then

    log_this INFO "Static Analysis TESTS: (pylint)"
    pylint --rcfile=.cicd_scripts/static_analysis/config/pylintrc_tests ${TEST_FILES}

else
    log_this INFO "Static Analysis TESTS: Nothing to check."
fi

if [[ "${SRC_DIRS}${TEST_FILES}" != "" ]]; then

    log_this INFO "Static Analysis ALL: (mypy)"
    mypy --config-file .cicd_scripts/static_analysis/config/mypy.ini ${SRC_DIRS} ${TEST_FILES}

else
    log_this INFO "Static Analysis ALL: Nothing to check."
fi
