# project-infrastructure-cicd

Repository for reusable CI/CD parts.
Sample usage is in [js-payments] project.

```yaml
include:
  - project: trustpayments-public/stjs/cicd-os
    ref: master
    file: /cicd_template.yml
```

## Table Of Contents

- [Pipeline Development - HOWTO](.cicd_docs/pipeline_development_howto.md)
- [Pipeline Steps - Automatic](.cicd_docs/pipeline_steps_automatic.md)
- [Pipeline Steps - Inherited](.cicd_docs/pipeline_steps_inherited.md)

---
[js-payments]: https://gitlab.com/trustpayments-public/stjs/js-payments-v3