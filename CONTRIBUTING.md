# How to contribute
This repository requires several secret tokens and private keys configured in order for the pipeline to run tests and validations on downstream projects. Because of this, external merge requests are going to fail and will not get automatically tested.

A better idea may be to simply create an [issue](https://gitlab.com/dashboard/issues) for our development team.