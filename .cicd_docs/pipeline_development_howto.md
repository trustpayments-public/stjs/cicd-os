# How to work with gitlab pipelines

## Rules
Every job in this pipeline can be disabled by specifying a `DISABLE_JOB_*` flag. E.g. for a job called 
`test-job`, you can set `DISABLE_JOB_test_job = "true"` and the job should be skipped when Gitlab
resolves rules for the pipeline.

Unfortunately, to achieve this, we had to introduce a "transformation" step, we called _"locking the pipeline"_. 
In the `.gitlab` directory in this repo, you'll find two folders:
- `pipeline` which is the source directory, and where you should make your edits
- `pipeline.lock` which contains _"locked"_ (or: transformed) versions of the pipeline scripts, in the format 
   understandable by Gitlab CI.
   
The reason for this is that we needed a way to manage and group job "rules" in a more pleasant way, which would 
otherwise not be possible, due to inability of merging lists using YAML Anchors, and Gitlab CICD `extend` keyword.

The "_"locking mechanism"_ allows us to keep nice-to-work-with, grouped rule-sets, while also being able to dynamically
add a rule on every job, that disables it based on dedicated Environment Variable.

### How to define rules for jobs?
The easiest way is to look at existing jobs, so let's discuss this on an example:
```yaml
test-job:
  extends:
    - .tags-sample
  rules: {@inject-rules:test_job:.rules-test}
  script:
    - echo "test job"
```

As you can see, we _"did something"_ to the `rules` attribute. It's still valid YAML (YAML parsers think the whole 
`{@injest-rules...}` macro is just a string), however it will not validate using Gitlab CI Lint. This macro however,
tells our _"locking mechanism"_ two things:
1. Define dynamic rule, that will disable this job if `[DISABLE_JOB_]test_job` Environment Variable is set to `"true"`.
2. Add all other rules specified in `.rules-test` rule-set.

The rule-sets are defined in `.gitlab/pipeline/rules.yml` and should look like this:
```yaml
.atomic-rules:

  ...

  - &otherwise
    when: on_success

  - &not-on-smoke-tests
    if: '$SMOKE_TESTS == "true"'
    when: never

  ...

.rules-test:
  rules:
    - <<: *not-on-smoke-tests
    - <<: *otherwise
```
As you can see, the convention is to _"name"_ atomic rules, and then attach them to a rule-set using YAML Anchors.
The _"locking mechanism"_ then takes the atomic rules out of rule-sets, prepends them with No.1 Rule allowing for 
disabling jobs, and puts the whole list in place of the macro in YAML file.

### When does _locking_ happen?
Locking happens on git `pre-commit` hook. In order to set-it-and-forget-it, simply install 
[pre-commit](https://pre-commit.com/), and run:
```bash
pre-commit install
```
in main repository folder. Additionally, you can run the tool manually by executing:
```bash
.cicd_scripts/pipeline/lock.sh
```
First execution of the tool might need more time, as it creates Python virtualenv and downloads dependencies.
After that, it should be a breeze on every commit.

### What if I commit my changes without locking?
The scripts need to be pre-generated in order for Gitlab to properly resolve pipeline structure, so the _locking_
will not be performed while the pipeline is running. What **is** performed however is validation if all the files
in `.gitlab/pipeline` are _in-sync_ with all the files in `.gitlab/pipeline.lock`. If they're not, the pipeline
will fail.

## Macros - full syntax
We currenly only offer this single macro, but the _"locking mechanism"_ can be extended in the future.

- `@inject-rules`  
  Syntax:  
  ```
   {@inject-rules:[<env_var_suffix>]:[<ruleset_id>[:<ruleset_id[...]]} 
  ```
  Where:
    - `<env_var_suffix>`   
      (Optional) suffix of the Environment Variable that will allow Projects to disable this 
      particular job. Use underscores instead of hyphens, but try to mimic the name of the job. 
      The Environment Variables follow the pattern: `DISABLE_JOB_<env_var_suffix>`, e.g. `DISABLE_JOB_tf_check`.
    - `<ruleset_id>`  
      (Optional, Many) names of rule-sets defined in `rules.yml` file. It is possible to specify more than one
      rule-set, in which case the rules from consecutive rule-sets will be added in order of appearance 
      or rule-sets in macro and of rules in each rule-set.
      
      **Note!**   
      1. If there are more than one rule-sets in the macro, only the last one should contain _unconditional_ rule 
         (i.e. a rule that only contains `when`). Otherwise, some rules will never be evaluated because of the 
         _unconditional_ rule in the middle of assembled stack.
      2. If you specify `<env_var_suffix>`, you **should** add at least one rule (_unconditional_ one) 
         with `<ruleset_id>`. Otherwise, gitlab will assume default `when: never` as closing condition and never run
         the job.

  Examples:
    - `{@inject-rules::.rules-when-releasing}`   
      This job cannot be disabled with an Environment Variable (no `<env_var_suffix>` defined), and will get rules
      from single rule-set.             
      
    - `{@inject-rules:build_job:.rules-when-review|.rules-when-renovate}`     
      This job can be disabled with an Environment Variable `DISABLE_JOB_build_job`, and will get 
      rules assembled by concatenating `rules:` lists from two specified rule-sets - first from `.rules-when-review`,
      then from `.rules-when-renovate`. Only the last of those rule-sets should contain _unconditional_ rule.                 

    - `{@inject-rules:}`  
      This job will never be executed. In this case you'd rather not specify `rules:` attribute at all, to make job
      run on default settings (if `rules:` is missing, the job will run `on_success`).
      
    - `{@inject-rules:copy-cicd-scripts:.rules-test}`
      This macro will fail the _"locking"_ script, because the `<env_var_suffix>` does not conform to Environment
      Variable name specification (contains hyphens instead of underscores).