# Including CICD template

In order for CICD to work, its template needs to be included in your `gitlab-ci.yml`. You should not include single files - if you don't want a job to run, you can either disable job either by setting `DISABLE_JOB_` variable (automatic jobs, see next section) or prepending job name with `.` (inherited jobs). 

Template file is called [`cicd_template.yml`](https://gitlab.com/trustpayments-public/stjs/cicd-os/-/blob/master/cicd_template.yml). 

Example usage in `gitlab-ci.yml`:
```yaml
.project-infrastructure-cicd-ref: &project-infrastructure-cicd-ref 0.0.304      # CICD version 

.project-infrastructure-cicd: &project-infrastructure-cicd
project: securetrading-gl/st-server-project/project-infrastructure-cicd
ref: *project-infrastructure-cicd-ref

include:
  - file: /cicd_template.yml
    <<: *project-infrastructure-cicd
```

# Automatic pipeline jobs

Automatic pipeline jobs are run for every pipeline automatically, unless they are explicitly disabled or omitted.

# Disabling jobs / activities

1. Temporarily disabling a job.

   This functionality may come handy when your project is not yet ready to run specific jobs. You may not be ready to
   run static code analysis (because your implementation is still rough and bumpy), specific kind of tests (because
   they do not exists yet) or execute production steps (because you're not done in general).

   In order to disable the job until you're ready, specify a `DISABLE_JOB_<job_name>` "global variable"_ in your
   `.gitlab-ci.yml` file.

1. Temporarily disabling activity.

   Similarly to job disabling, you can disable an `Activity`, a feature which is enabled for job or multiple jobs.

   Examples:
   ```yaml
   DISABLE_ACTIVITY_docker_image_build_cache: 'true'
   ```

# Automatic pipeline jobs - parameters
Automatic jobs don't get inherited, so you cannot specify parameters for them when extending them. Instead, automated
jobs will either operate on conventions, or utilize _"Global Variables"_ to configure themselves. Below, you'll find
a list of jobs, and if a job on that list specifies "Parameters" it is assumed (unless explicitly specified otherwise)
 that those parameters are expected to be passed as _"Global Variables"_.

# List of automatic pipeline jobs for gitlab stages:
- [Automatic pipeline jobs](#automatic-pipeline-jobs)
- [Disabling jobs / activities](#disabling-jobs--activities)
- [Automatic pipeline jobs - parameters](#automatic-pipeline-jobs---parameters)
- [List of automatic pipeline jobs for gitlab stages:](#list-of-automatic-pipeline-jobs-for-gitlab-stages)
  - [initialize](#initialize)
    - [copy-cicd-scripts](#copy-cicd-scripts)
  - [pre-build](#pre-build)
    - [docker-images-pull](#docker-images-pull)
    - [docker-images-build](#docker-images-build)
  - [fast-tests](#fast-tests)
    - [unit-tests](#unit-tests)
    - [ts-static-analysis](#ts-static-analysis)
    - [py-static-analysis](#py-static-analysis)
    - [docker-static-analysis](#docker-static-analysis)
    - [vuln-check](#vuln-check)
  - [validate-merge-request-description](#validate-merge-request-description)
  - [release](#release)
  - [post-release](#post-release)
    - [npm-publish](#npm-publish)
  
## initialize

### copy-cicd-scripts
Copies all `.cicd_scripts` from CICD repository to actual build directory, to be able to execute all scripts required by CICD.

## pre-build

### docker-images-pull
Enabled by default for review branches. Pulling `base` and `pipeline` master images and tag them as own.

It's faster than building images.

If you would make any changes in images and you want to `force` rebuilding on review branch then set:

```
BUILD_IMAGES: "true"
```

To enable [docker-images-build](#docker-images-build) instead this one.

After merge you should comment/delete this line and push again, to keep your review branches using faster [docker-images-pull](#docker-images-pull) job.

### docker-images-build
Builds `base` and `pipeline` images.

By default executed only on `master` branch, to keep packages updated & upgraded  on every release.

## fast-tests

### unit-tests
Performs Unit Testing using [Jest](https://jestjs.io/). It requires NPM script named `coverage` to be placed in project's `package.json` which should trigger Jest (example script: `jest --coverage`).

It can be configured using [configuration file](https://jestjs.io/docs/en/configuration.html) (`jest.config.js`) or in `package.json`.

This job can be disabled by setting variable `DISABLE_JOB_unit_tests` to `true` in `.gitlab-ci.yml`.

### ts-static-analysis
Perform static code analysis and automatically formats the code using [Prettier](https://www.npmjs.com/package/prettier). It requires NPM script named `prettier` to be placed in project's `package.json` (example script: `./node_modules/.bin/prettier --write ./src/**/*.{ts,js,css,json}`).

Prettier [can be configured in many ways](https://prettier.io/docs/en/configuration.html), ex. using configuration file (`.prettierrc`). For [ignoring files](https://prettier.io/docs/en/ignore.html), please use `.prettierignore` file placed in project root directory.

This job can be disabled by setting variable `DISABLE_JOB_ts_static_analysis` to `true` in `.gitlab-ci.yml`.

### py-static-analysis

Performs static code analysis using `pylint`, `mypy` and `bandit`. The configuration files for those tools are common
across all projects, and stored in [`static_analysis_config`](.cicd_scripts/static_analysis/config).

Static analysis has been grouped by the source and tests, as you can see in the parameters section's `PROJECT_PYTHON_`
variables, since `pylint` needs a looser config for tests and `bandit` is only suitable for the source. `mypy` config is used for both.

Parameters:
- `PROJECT_PYTHON_SOURCES` - (Optional) whitespace-separated list of directories containing python source code.
- `PROJECT_PYTHON_TESTS` - (Optional) whitespace-separated list of directories containing python test code.

Convention:
- In addition to `PROJECT_PYTHON_SOURCES` and `PROJECT_PYTHON_TESTS`, the job also automatically looks for following sources:
  * `scripts/python`
  * `src`
  * `tests`

### docker-static-analysis

Running [hadolint](https://github.com/hadolint/hadolint) to lint Dockerfiles in repository. By default, it checks `docker/` folder and uses global [config file](https://github.com/hadolint/hadolint#configure) located in `.cicd_scripts/statis_analysis/config/.hadolint.yml` for ignores and trusted repositories. This can be overridden by setting variables: 
```
DOCKER_LINT_SCAN_PATH: `path_relative_to_project_root`
DOCKER_LINT_CONFIG_FILE: `path_relative_to_project_root`
``` 

To disable this job set:
```
DISABLE_JOB_docker_static_analysis: 'true'
```

### vuln-check
Performs dependencies' vulnerability check using [Snyk](https://snyk.io/). It requires NPM script `snyk` to be placed in project's `package.json` (example script: `snyk test --org=secure-trading`).

You can manage which vulnerabilities can be patched or ignored through Snyk policy file (`.snyk` in project root directory).

This job can be disabled by setting variable `DISABLE_JOB_vuln_check` to `true` in `.gitlab-ci.yml`.

## validate-merge-request-description
Performs validation of Merge Request description, if any Merge Request is currently open for the branch. If description does not match validation criteria, the pipeline will fail.

This job can be disabled by setting variable `DISABLE_JOB_validate_merge_request_description` to `true` in `.gitlab-ci.yml`.

### Validation criteria
- Every Merge Request should contain at least one of the following headers:
    ```
    # Changes
    # Breaking changes
    # New features
    # Hotfix
    # Fixes
    ```
  Empty headers are not considered valid.

## release

### release-generate-version
This job create release artifact and tags Git ref with the new semantic version prefixed with 'upcoming-', ex `upcoming-0.1.0`.
For generating new version tag, it scans all release tags (including 'upcoming' ones) and increment the latest using semantic version in form `MAJOR.MINOR.PATCH`.

You can run pipeline with variable `VERSION_PART_TO_UPDATE` and specify which version part should be incremented.

You can choose:
* `MAJOR`
* `MINOR`
* `PATCH`

By default `VERSION_PART_TO_UPDATE` is set to `PATCH`.

Result of this job is git tag `upcoming-MAJOR.MINOR.PATH` and stored release artifact containing version number (without 'upcoming-' prefix), release notes description, release branch name.

Release notes have two versions set by a variable `ARTIFACT_RELEASE_DESCRIPTION_TYPE`:
- `BASIC` (default) - commit history since last full release
    ```
    # Commit history
      - af38ah2 - Some changes
      - 3b23hea - Some more changes
    ```
- `EXTENDED` - includes descriptions all merge requests since last full release (if not `renovate`), formatting them as shown below:
    ```
    # <MR Link> - <MR Title>                                    // !48 - Awesome MR that changes things.
    > <Blockquoted MR Contents, but not for Renovate>                        
    ```
    
    After merge requests descriptions, it renders commit history since last full release:
    ```                                                      
    # Commit history                                            // Always - required by PCI/DSS.
      - af38ah2 - Some changes
      - 3b23hea - Some more changes
    ```

### release
Release use release artifact generated by `release-generate-version` job to create new GitLab Release. (Under the hood Gitlab creates also Git Tag).

## npm-publish
NPM packages which has been generated in [release](#release) will be published to NPM package registry.

## generate-version-files

Those jobs are executed in dedicated pipeline only if variable is set: (similar to smoke tests pipeline)
```UPDATE_VERSION_FILES: 'true'```
