# Inherited pipeline jobs
Some activities in the Pipeline are not automatically triggered, and require additional parameters to run. For such 
cases, the pipeline exposes [hidden jobs][gitlab_hidden_jobs] that you can inherit from using `extends` keyword.

# List of inherited / extendable pipeline jobs for gitlab stages:
- [build](#build)
  - [.utility-images-build-*](#utility-images-build-)
  - [.android-pipeline-build-*](#android-pipeline-build-)
- [fast-tests](#fast-tests)
  - [zap-baseline-scan-*](#zap-baseline-scan-)
- [extended-tests](#extended-tests)
  - [.mobsf-static-analysis](#mobsf-static-analysis)
 
# List of jobs regarding CDN:
- [.trigger-publish-to-cdn-*](#trigger-publish-to-cdn-)

## build

### .utility-images-build-*

Build utility images which will be pushed into [GitLab Container Registry][gitlab_container_registry].

Can be used to build mock images.

Parameters:
- `UTILITY_NAME` - (Mandatory) name of the utility
- `USE_LOG_LEVEL_ARG` - (Optional) set to ```true``` if you want use ```ARG LOG_LEVEL``` in your Dockerfile
- `INJECT_TO_PROJECT_IMAGES` - (Optional) set to `true` if you want use `ARG IMAGE_BASE/IMAGE_PIPELINE` in your Dockerfile
- `BUILD_ARGS_EXTRA` - (Optional) set all extra build args, eg: ```--build-arg MY_ARG=${MY_ARG_VALUE}``` or ```--build-arg MY_ENV_VAR```
- `DOCKERFILE_PATH` - (Optional) use specific Dockerfile path instead default ( ```docker/${UTILITY_NAME}/Dockerfile``` )
- `DOCKER_TARGET` - (Optional) use specific target instead default ( value of ```UTILITY_NAME``` )
- `BUILD_EXECUTION_PATH` - (Optional) execute docker build and push from specified directory

### .android-pipeline-build-*
Build images for android which will be pushed into [GitLab Container Registry][gitlab_container_registry].

Parameters:
- `UTILITY_NAME` - (Mandatory) name of the utility
- `USE_LOG_LEVEL_ARG` - (Optional) set to ```true``` if you want use ```ARG LOG_LEVEL``` in your Dockerfile
- `INJECT_TO_PROJECT_IMAGES` - (Optional) set to `true` if you want use `ARG IMAGE_BASE/IMAGE_PIPELINE` in your Dockerfile
- `BUILD_ARGS_EXTRA` - (Optional) set all extra build args, eg: ```--build-arg MY_ARG=${MY_ARG_VALUE}``` or ```--build-arg MY_ENV_VAR``` e.g.
    - `ANDROID_CMDLINE_TOOLS_VERSION` - if version set ANDROID cmdline tools will be installed on image otherwise installation will be skipped, default `not set`
    - `GRADLE_VERSION` - if version set GRADLE will be installed on image otherwise installation will be skipped, default `not set`
    - `KOTLIN_VERSION` - if version set KOTLIN will be installed on image otherwise installation will be skipped, default `not set`
- `BUILD_EXECUTION_PATH` - (Optional) execute docker build and push from specified directory

[gitlab_container_registry]: https://gitlab.com/help/user/packages/container_registry/index

## fast-tests

### zap-baseline-scan-*

Perform Baseline Scan using [ZAP](https://www.zaproxy.org/).

You can manage which warnings can ignored in `.cicd_scripts/security/zap/zap-baseline-scan.conf` file. You can pass different config to the script by using `ZAP_CONFIG_PATH` optional parameter.

More about Baseline Scan can be found here [Baseline Scan](https://www.zaproxy.org/docs/docker/baseline-scan/).

Parameters:
- `URL_TO_SCAN` - (Mandatory) Url to endpoint being tested
- `ZAP_CONFIG_PATH` - (Optional) use specific ZAP config path instead of default one ( ```.cicd_scripts/security/zap/baseline-scan.conf``` )

## extended-tests

### .mobsf-static-analysis

Perform security static analysis using [MOBSF](https://github.com/MobSF/Mobile-Security-Framework-MobSF).

Parameters:
- `MOBSF_TEST_FILE` - (Mandatory) Path to tested file (supported files: APK/XAPK/IPA/ZIP/APPX)
- `MOBSF_PDF_REPORT` - (Optional) File name for JSON report (default: security-report.pdf)
- `MOBSF_JSON_REPORT` - (Optional) File name for JSON report (default: security-json.pdf)
- `MOBSF_SECURITY_MIN_SCORE` - (Optional) Minimal score result for scan (default: 100)


## trigger-publish-to-cdn-*
Those jobs are triggering CDN pipeline which will use artifact from existing pipeline and publish it to CDN.

Parameters:
- `PUBLISH_ARTIFACT_SOURCE_JOB_NAME` - (Mandatory) Job name from existing pipeline where artifact is exposed
- `PUBLISH_ARTIFACT_SOURCE_PATH` - (Mandatory) Path to a file inside the artifacts
- `PUBLISH_ARTIFACT_TARGET_RELATIVE_PATH ` - (Mandatory) Path where artifact will be stored. Final path will be prefixed depends from project which will trigger that pipeline
- `PUBLISH_ARTIFACT_DRY_RUN ` - (Optional) 'true' if you want to test all but upload, by default set to 'false'

Example usage:
```yaml
publish-artifact-to-cdn:
  extends: .trigger-publish-to-cdn-release
  stage: 'post-release'
  variables:
      PUBLISH_ARTIFACT_SOURCE_JOB_NAME: 'build-release'
      PUBLISH_ARTIFACT_SOURCE_PATH: 'my_lib.js'
      PUBLISH_ARTIFACT_TARGET_RELATIVE_PATH: 'my_lib.js'
```